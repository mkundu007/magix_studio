from django.shortcuts import render

import os
import uuid,bcrypt
import epoch
import base64

from django.contrib.auth import authenticate, login as dj_login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.contrib.auth.models import User
from users.models import UserProfile
from django.conf import settings


'''
    @name: index
    @param : request 
    @description : index page for users and visitors
'''   
def index(request):
    return render(request, 'index.html' , {'title': 'Home | Magix Studio'})


'''
    @name: sign_up
    @param : request 
    @description : registration page and registration view for upcoming users
'''   
def sign_up(request,argm):
    if request.method == 'POST' and argm=='action':
        if request.user.is_anonymous:
            try:
                firstname = request.POST.get('firstname',None)
                lastname = request.POST.get('lastname',None)
                mail = request.POST.get('email',None)
                password = request.POST.get('password',None)
                
                if firstname is not None and mail is not None and password is not None:
                    if UserProfile.objects.filter(email = mail).exists():
                        return HttpResponse("This email is already registered", status = 406)
                    else:
                        ''' @process: creating username ''' 
                        first_3_letter_from_firstname = firstname[0:3]
                        random_number = epoch.now()
                        random_number = str(random_number)
                        username = first_3_letter_from_firstname + random_number + "-magix"
                        
                        user=User.objects.create_user(
                            username = username,
                            first_name = firstname,
                            last_name = lastname,
                            email = mail,
                            password = password
                        )
                        user.is_active=True
                        user.save()
                        
                        key = str(uuid.uuid4())+str(uuid.uuid4())+str(uuid.uuid4())
                        UserProfile(user = user, email = mail, confirm_key = key).save()
                        
                        return HttpResponse("Registration is successful", status = 201)
                else:
                    return HttpResponse("Please fill up the form properly", status = 417)
            except Exception as e:
                return HttpResponse("Some error occurred", status = 400)
        else:
            return HttpResponse("FORBIDDEN", status = 403)
    else:
        if request.user.is_anonymous and argm is not None:
            return render(request, 'visitors/sign_up.html' ,
                          {'title': 'Sign Up | Magix Studio','package':argm})
        else:
            return HttpResponseRedirect('/dashboard/')


'''
    @name: login
    @param : request 
    @description : login page and login view for users
''' 
def login(request):
    if request.method == 'POST':
        if request.user.is_anonymous:
            try:
                email = request.POST.get('email')
                password = request.POST.get('password')
                next = request.POST.get('next')
                redir = next if next != 'None' else '/dashboard/'
                
                if UserProfile.objects.filter(email = email).exists():
                    username = User.objects.get(email = email.lower()).username
                    user = authenticate(username = username, password = password)
        
                    if user is not None:
                        dj_login(request, user)
                        request.session.set_expiry(3600)
                        return HttpResponse(redir, status = 200) 
                    else:
                        return HttpResponse("Password is invalid", status = 406) 
                else:
                    return HttpResponse("This email is not registered", status = 406)  
            except Exception as e:
                    return HttpResponse("Some error occurred", status = 400)  
        else:
            return HttpResponse("FORBIDDEN", status = 403)
    else:
        if request.user.is_anonymous:
            return render(request, 'visitors/login.html' ,
                          { 'next': request.GET.get('next', None),'title': 'Login | Magix Studio'})
        else:
            return HttpResponseRedirect('/dashboard/')



# '''
#     @name: DoSignUp
#     @param : post 
#     @description :  Rest API for sign up
# ''' 
# class DoSignUp(APIView):
#     def post(self, request, format = None):
#         if request.user.is_anonymous:
#             try:
#                 firstname = request.POST.get('firstname')
#                 lastname = request.POST.get('lastname')
#                 email = request.POST.get('email')
#                 password = request.POST.get('password')
#                 
#                 if firstname is not None and email is not None and password is not None:
#                     if UserProfile.objects.filter(identical_email = email).exists():
#                         return Response({"response":"exists"}, status = status.HTTP_403_FORBIDDEN)
#                     else:
#                         ''' @process: creating username ''' 
#                         first_3_letter_from_firstname = firstname[0:3]
#                         random_number = epoch.now()
#                         random_number = str(random_number)
#                         username = first_3_letter_from_firstname + random_number + "-magix"
#                         
#                         user=User.objects.create_user(
#                             username = username,
#                             first_name = firstname,
#                             last_name = lastname,
#                             email = email,
#                             password = password
#                         )
#                         user.is_active=True
#                         user.save()
#                         
#                         key = str(uuid.uuid4())+str(uuid.uuid4())+str(uuid.uuid4())
#                         UserProfile(user = user, identical_email = email, confirm_key = key).save()
#                         
#                         return Response({"response":"success"}, status = status.HTTP_201_CREATED)
#                 else:
#                     return Response({"response":"error"}, status = status.HTTP_417_EXPECTATION_FAILED)
#             except Exception as e:
#                 return Response({"response":"failure"}, status = status.HTTP_400_BAD_REQUEST)
#         else:
#             return Response({"response":"invalid"}, status = status.HTTP_403_FORBIDDEN)