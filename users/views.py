import os,sys
import uuid,bcrypt
import epoch
import json
import re
import base64
import users.tests as myfunc
import users.fontfamilies as fontfamilies
import urllib.request
import gcsfs
import imageio

from django.contrib.auth import authenticate, login as dj_login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponse, HttpResponseRedirect, HttpResponsePermanentRedirect
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.http.response import JsonResponse
from django.contrib.auth.models import User
from django.contrib import messages
from users.models import *
from django.conf import settings
from django.template.context_processors import media
from django.core.files.storage import default_storage
from django.core.exceptions import ObjectDoesNotExist
from moviepy.editor import *
from datetime import datetime
from django.core.files.base import ContentFile
from google.cloud import storage
from io import BytesIO
from PIL import Image
import shutil
import logging

logger = logging.getLogger(__name__)

# Google cloud storage api bucket declaration
gclient = storage.Client()
bucket = gclient.get_bucket(settings.GC_BUCKET)

'''
    @name: dashboard
    @param : request 
    @description : dashboard page for users
'''   
@login_required 
def dashboard(request):
    return render(request, 'users/dashboard.html' , {'title': 'Dashboard | Magix Studio'})


'''
    @name: project_showcase
    @param : request 
    @description : select template page for users
''' 
@login_required
def my_projects(request):
    appuser = UserProfile.objects.get(user = request.user)
    projects = Projects.objects.filter(
        appuser=appuser,del_flag=False
    ).order_by("-id")
    
    return render(
        request, 
        'users/project_showcase.html', 
        {
            'title': 'My Projects | Magix Studio',
            'projects': projects,
            'projects_count': projects.count
        }
    ) 
    
    
'''
    @name: select_template
    @param : request 
    @description : select template page for users
''' 
@login_required
def select_template(request):
    categories = Templates.objects.all()
    return render( 
        request,
        'users/select_template.html',
        {   
            'media_root': settings.MEDIA_ROOT_SERV,
            'cats': categories
        }
    )  
    
    
'''
    @name : set_template
    @input: get
    @description : sets template & redirects to editor page

''' 
@login_required 
def set_template(request,tid):
    try:
#         temp=Templates.objects.filter(temp_id=tid)
#         if not temp:
#             return HttpResponseRedirect('/error/')
#         else:
        logger.info(settings.IF_CLOUD)
        logger.info(settings.MEDIA_ROOT)

        temp_of = default_storage.open(os.path.join(settings.MEDIA_ROOT,'templates/'+tid
                                                    +'/local.json'),'r')
        user_temp = ContentFile(temp_of.read())
        user_temp_json="uploaded_media/" + str(request.user) + "/"+str(epoch.now())+".json"
        user_temp_fp = os.path.join(settings.MEDIA_ROOT,user_temp_json)
        default_storage.save(user_temp_fp,user_temp) 
        
        # save to the model
        ''' what will happen if request user will come none or annynomous user, '''
        proj_obj = Projects(
            appuser = UserProfile.objects.get(user = request.user),
            preview = settings.MEDIA_ROOT_SERV+'templates/'+tid+'/preview.jpg',
            title = 'My Magix Project',
            json = user_temp_json,
        )
        proj_obj.save()
        
        return HttpResponseRedirect('/project/editor/'+str(proj_obj.id)+'/')
            
    except Exception as e:
        return HttpResponse("Error 400 : Please contact support if error persists.", status = 400) 
 
    
'''
    @name: editor
    @param : request 
    @description : editor page for users
''' 
@login_required
def editor(request,pid):
    try:
        appuser = UserProfile.objects.get(user = request.user)
        project = Projects.objects.filter(appuser=appuser, id=pid, del_flag=False)

        if not project :
            return HttpResponseRedirect('/project/select-template/')
        else :
            project = project.first()
            media_img_lib = UserMedia.objects.filter(
                appuser = appuser, 
                media_type = 'media_image'
            ).order_by("-id")
        
            media_watermark_lib = UserMedia.objects.filter(
                appuser = appuser, 
                media_type = 'media_watermark'
            ).order_by("-id")
        
            media_audio_lib = UserMedia.objects.filter(
                 appuser = appuser, 
                 media_type = 'media_audio'
            ).order_by("-id")
        
            media_video_lib = UserMedia.objects.filter(
                appuser = appuser, 
                media_type = 'media_video'
            ).order_by("-id")
      
            with open(os.path.join(settings.MEDIA_ROOT,project.json)) as json_file:  
                template = json.load(json_file)
            
            return render(
                request, 
                'editor/index.html', 
                {
                    'title': 'Editor | Magix Studio', 
                    'media_root': settings.MEDIA_ROOT_SERV,
                    'template': template,
                    'project': project, 
                    'media_img_lib': media_img_lib,
                    'media_watermark_lib': media_watermark_lib,
                    'media_audio_lib': media_audio_lib,
                    'media_video_lib': media_video_lib,
                    'media_img_count': media_img_lib.count,
                    'media_watermark_count': media_watermark_lib.count,
                    'media_audio_count': media_audio_lib.count,
                    'media_video_count': media_video_lib.count,
                    'font_family': fontfamilies.members
                }
            )  
            
    except Exception as e:
        return HttpResponse("Error 400 : Please contact support if error persists.", status = 400) 
    
    
'''
    @name: editor1 (TEST CASE)
    @param : request 
    @description : editor1 page for users
''' 
@login_required
def editor1(request,pid):
    try:
        appuser = UserProfile.objects.get(
            user = request.user
        )
        project = Projects.objects.filter(appuser=appuser,id = pid,del_flag=False)
        if not project :
            return HttpResponseRedirect('/project/select-template/')
            
        else :
            project = project.first()
            media_img_lib = UserMedia.objects.filter(
                appuser = appuser, 
                media_type = 'media_image'
            ).order_by("-id")
        
            media_watermark_lib = UserMedia.objects.filter(
                appuser = appuser, 
                media_type = 'media_watermark'
            ).order_by("-id")
        
            media_audio_lib = UserMedia.objects.filter(
                 appuser = appuser, 
                 media_type = 'media_audio'
            ).order_by("-id")
        
            media_video_lib = UserMedia.objects.filter(
                appuser = appuser, 
                media_type = 'media_video'
            ).order_by("-id")
            
            f = open(os.path.join(settings.MEDIA_ROOT,project.json), 'r')
            template = f.read()
            f.close()
            
            return render(
                request, 
                'editor/index.html', 
                {
                    'title': 'Editor | Magix Studio', 
                    'template': template,
                    'project':project, 
                    'media_img_lib': media_img_lib,
                    'media_watermark_lib': media_watermark_lib,
                    'media_audio_lib': media_audio_lib,
                    'media_video_lib': media_video_lib,
                    'media_img_count': media_img_lib.count,
                    'media_watermark_count': media_watermark_lib.count,
                    'media_audio_count': media_audio_lib.count,
                    'media_video_count': media_video_lib.count,
                    'font_family': font_family
                }
            )  
    except Exception as e:
        return HttpResponse("Error 400 : Please contact support if error persists.", status = 400) 
    
    
    
        
'''
    @name: video_viewer
    @param : request 
    @description : video view, share & comment page for visitors
''' 
def video_viewer(request,vid):
    if request.user.is_anonymous:
        appuser = None
    else:
        appuser = UserProfile.objects.get(user = request.user)
        
    project = Projects.objects.filter(id = vid, del_flag = False)
    
    if not project :
        return HttpResponse("Error 400 : Please contact support if error persists.", status = 400) 
    else :
        project = project.first()
        if_owner = True if appuser == project.appuser else False
        
        owner = User.objects.get(id = project.appuser.id)
        
        return render(
            request, 
            'users/video_viewer.html', 
            {
                'project': project,
                'owner': owner,
                'if_owner': if_owner,
            }
        )


'''
    @name: logout
    @param : request 
    @description : logout view for users
''' 
@login_required 
def logout_view(request):
    request.session.flush()
    return HttpResponseRedirect('/')
    
    
##=========================================== Rest APIs ===========================================##    
    
'''
    @name: modify project APIView
    @param : post 
    @description : modify project view for users
''' 
class ModifyProject(APIView):
    def post(self, request, format = None):
        try:
            ''' get is used only for getting value using unique data, if you passing 2 parameter, then it should be filter '''
            proj_obj = Projects.objects.get(id=request.POST.get('id'))
            if request.POST.get('action')=='title':
                # save to the model
                proj_obj.title=request.POST.get('title',None)
                proj_obj.save()
                return Response({"response" : "success"}, status = 200)
            elif request.POST.get('action')=='json':
                # save to the json file
                with open(os.path.join(settings.MEDIA_ROOT,proj_obj.json), 'w') as outfile:  
                    json.dump(request.POST.get('json'), outfile)
                return Response({"response" : "success"}, status = 200)
            else:
                return Response({"response": "failure"}, status = 417)
        except Exception as e:
            return Response({'response':str(e)}, status = 400)
    
        
'''
    @name: media upload APIView
    @param : request 
    @description : media upload view for users
''' 
class MediaUploadApi(APIView):
    def post(self, request, format = None):
        if request.user.is_anonymous:
            return Response({"response":"FORBIDDEN"}, status = 403)
        else:
            if request.FILES:
                try:
                    file_temp = request.FILES['file']
                    file_name = str(epoch.now()) + os.path.splitext(str(file_temp))[1]
                    media_id = 0
                    media_url = " "
                    media_type = request.POST.get('uploadtype',None)
                    duration = " "
                    thumb_url = settings.MEDIA_ROOT+"uploaded_media/music.png"
                    media_path = settings.MEDIA_URL+"uploaded_media/"+str(request.user)+'/'+file_name
                    
                    target_gc_object = bucket.blob(media_path)
                    target_gc_object.upload_from_string(file_temp.read(),
                                                        content_type=file_temp.content_type)
                    media_url = target_gc_object.public_url
                    
                    if ( media_type == "media_audio"):
                        # save audio using google cloud storage
                        clip = AudioFileClip(media_url) #urllib.request.urlopen(media_url).read()
                        duration = str(clip.duration)

                        # save to the model | we need to store this 'media_object' before save(), because we need media_obj.id later
                        media_obj = UserMedia(
                            appuser = UserProfile.objects.get(user=request.user),
                            media = media_url + "," + duration,
                            media_type = media_type
                        )
                        media_obj.save()
                        media_id = media_obj.id
                        
                    else:
                        # save to the model
                        media_obj = UserMedia(
                            appuser = UserProfile.objects.get(user=request.user),
                            media = media_url,
                            media_type = media_type
                        )
                        media_obj.save()
                        media_id = media_obj.id
                                        
                    if (media_type == "media_image"):
                        return Response({"media_id": media_id, "path": media_url, "type": media_type},
                                        status = 200)
                    elif (media_type == "media_watermark"):
                        return Response({"media_id": media_id, "path": media_url, "type": media_type},
                                        status = 200)
                    else:
                        return Response({"media_id": media_id, "path": media_url, "thumbnail": thumb_url},
                                    status = 200)
                except Exception as e:
                    return Response({'response':str(e)}, status = 400)
            else:
                return Response({"response": "failure"}, status = 417)
            
'''
    @name: video upload APIView
    @param : request 
    @description : video upload view for users
''' 
class VideoUploadApi(APIView):
    def post(self, request, format = None):
        if request.user.is_anonymous:
            return Response({"response":"FORBIDDEN"}, status = 403)
        else:
            if request.FILES:
                try:
                    file_temp = request.FILES['file']
                    media_path =  settings.MEDIA_ROOT
                    file_name = str(epoch.now())
                    video_file = file_name + os.path.splitext(str(file_temp))[1]
                    thumb_file = file_name + '.gif'
                    video_path = "uploaded_media/" + str(request.user) + "/"+ video_file
                    thumb_path = "uploaded_media/" + str(request.user) + "/"+ thumb_file
                    
                    target_gc_object = bucket.blob(settings.MEDIA_URL+video_path)
                    target_gc_object.upload_from_string(file_temp.read(),
                                                        content_type=file_temp.content_type)
                    video_url = target_gc_object.public_url
                    
                    clip = VideoFileClip(video_url)
                    
                    width = str(clip.w)
                    height = str(clip.h)
                    duration = str(clip.duration)
                    
                    thumb_url=settings.MEDIA_ROOT+'images/sample1.jpg'
                    rratio=400/clip.h
                    gifClip = clip.subclip(8, 10).resize(height=clip.h*rratio,width=clip.w*rratio)
                    
                    gifClip.write_gif(os.path.join(media_path,thumbnail),fps=5)
                    
                    clip.close()
                    
                    # save to the model | we need to store this 'media_object' before save(), because we need media_obj.id later
                    media_obj = UserMedia(
                        appuser = UserProfile.objects.get(user=request.user),
                        media = video_url + "," + thumb_url + "," + duration + "," + width + "," + height,
                        media_type = 'media_video'
                    )
                    media_obj.save()
                    media_id = media_obj.id
                    
                    return Response({
                        "media_id": media_id, 
                        "type": "media_video", 
                        "path": video_url, 
                        "thumbnail": thumb_url, 
                        "duration": duration,
                        "width": width,
                        "height": height
                    }, status = 200)
                except Exception as e:
                    print(str(e))
                    return Response({'response':str(e)}, status = 400)
            else:
                return Response({"response": "failure"}, status = 417)
 
        
'''
    @name: media delete APIView
    @param : request 
    @description : media delete view for users
''' 
class MediaDeleteApi(APIView):
    def post(self, request, format = None):
        if request.user.is_anonymous:
            return Response({"response":"FORBIDDEN"}, status = 403)
        else:
            try:
                media_id = request.POST.get('media_id',None)
                media_obj = UserMedia.objects.filter(id = media_id).first()
                
                if (media_obj):
                    if ( media_obj.media_type == "media_video") :
                        media_path = media_obj.media.split(',')[0]
                        thumbnail_path = media_obj.media.split(',')[1]
                        os.unlink(os.path.join(settings.MEDIA_ROOT, media_path))
                        os.unlink(os.path.join(settings.MEDIA_ROOT, thumbnail_path))
                        media_obj.delete()        
                    else:
                        media_path = media_obj.media
                        file_to_delete = bucket.blob(media_path.replace(settings.MEDIA_ROOT,settings.MEDIA_URL))
                        file_to_delete.delete()
                        media_obj.delete()
                    return Response({"response": "success"}, status = 200)
                else:
                    return Response({"response": "failure"}, status = 417)          
            except Exception as e:
                print(str(e))
                return Response({'response':str(e)}, status = 400)

    
        
'''
    @name: save snaps APIView
    @param : request 
    @description : save snaps view for users
''' 
class SaveSnaps(APIView):
    def post(self, request, format = None):
        if request.user.is_anonymous:
            return Response({"response":"FORBIDDEN"}, status = 403)
        else:
            try:
                snaps = json.loads(request.POST.get('snaps',None))
                snaps_dir = os.path.join(settings.MEDIA_ROOT,"uploaded_media/" + str(request.user),
                                         str(request.POST.get('pid')),str(request.POST.get('ind')))
                
                if os.path.isdir(snaps_dir) == False :
                    os.mkdir(snaps_dir)
                else :
                    shutil.rmtree(snaps_dir)
                    
                for snap in snp :
                    snap_file='/'+str(epoch.now())+".png"
                    #default_storage.save(snap_file,snap) 
                        
                    imgstr=re.search(r'data:image/png;base64,(.*)',snap).group(1)
                    output=open(current_frames_dir+snap_file, 'wb')
                    decoded=base64.b64decode(imgstr)
                    output.write(decoded)
                    output.close()
                    
                    snaps_li.append(snap_file)
                    
                snaps_list = [ImageClip(current_frames_dir+m).set_duration(1/30) for m in snaps_li]
                vpath = current_frames_dir+'/temp.mp4'
                merged_clip = concatenate_videoclips(snaps_list)
                merged_clip.write_videofile(vpath,fps=90)
                            
                if request.POST.get('end') == 1 :
                    final_clip = myfunc.stitch_snaps(snaps_dir,request.POST.get('pid'))
                    thisProject=Projects.objects.get(id=request.POST.get('pid'))
                    thisProject.status=1
                    thisProject.save()
                
                return Response({"response": "success"}, status = 200)
            except Exception as e:
                return Response({'response':str(e)}, status = 400)
