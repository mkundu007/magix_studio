from django.db import models
from django.contrib.auth.models import User

# UserProfile Class
class UserProfile(models.Model):
    '''
    This is UserProfile class. It stores the profile information of
    registered user. This class inherits the auth user base class.
    '''
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.EmailField(null=False, blank=True)
    mobile_no = models.IntegerField(null=True, blank=False)
    purchase_pack = models.CharField(max_length=200, null=True, blank=False)
    confirm_key = models.CharField(max_length=200, null=False, blank=True)
    
    class Meta:
      db_table = "magix_users"

# UserProfile Class
'''
class UserProfile(User):
    identical_email = models.EmailField(null=False, blank=True)
    mobile_no = models.IntegerField(null=True, blank=False)
    purchase_pack = models.CharField(max_length=200, null=True, blank=False)
    confirm_key = models.CharField(max_length=200, null=False, blank=True)
'''
    
# UserMedia Class
class UserMedia(models.Model):
    '''
    This is UserMedia class. It stores the uploaded media of
    registered user.
    '''
    appuser=models.ForeignKey(UserProfile,on_delete=models.CASCADE)
    media = models.CharField(max_length=2000,null=True, blank=True)
    media_type = models.CharField(max_length=50,null=True, blank=True)
    favourite_flag = models.BooleanField(default=False)
    del_flag = models.BooleanField(default=False)
    
    class Meta:
      db_table = "magix_media"
    
# Projects Class
class Projects(models.Model):
    '''
    This is Projects class. It stores all the projects of the
    registered users.
    '''
    appuser=models.ForeignKey(UserProfile,on_delete=models.CASCADE)
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=30)
    preview = models.CharField(max_length=100)
    status = models.IntegerField(default=0)
    json = models.CharField(max_length=100)
    del_flag = models.BooleanField(default=False)
    
    class Meta:
      db_table = "magix_projects"
    
	
# Templates Class
class Templates(models.Model):
    cat_id = models.AutoField(primary_key=True)
    cat_title = models.CharField(max_length=100,null=True, blank=True)
    cat_temps = models.TextField(default=1)
    
    class Meta:
      db_table = "magix_templates"
    
    
