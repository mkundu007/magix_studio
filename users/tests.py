import os
from django.test import TestCase
from moviepy.editor import *

# Create your tests here.

'''
    @name: stitch snaps
    @param : request 
    @description : stitch snaps method
''' 
def stitch_snaps(frames_dir,pid):
    try:
        frames = os.listdir(frames_dir)
        clips_list=[]
        
        for frame in frames:
            clips_list.append(VideoFileClip(frames_dir+'/'+frame+'/temp.mp4'))
            
        final_video_path = frames_dir+'/'+pid+'_final.mp4'
        merged_clip = concatenate_videoclips(clips_list)
        merged_clip.write_videofile(final_video_path,fps=90)
        
        gifClip = merged_clip.subclip(3, 5)
        gifClip.write_gif(frames_dir+'/'+pid+'_final.gif',fps=5)
        
        return final_video_path
    except Exception as e:
        return str(e)