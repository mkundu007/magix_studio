//Check if user client is online
function checkconnection() { 
	var status = navigator.onLine; 
	if (status) { 
		$('#networkDetectionModal').modal('hide');
		$('.network-detect').fadeOut();
	}
	else {
		$('#networkDetectionModal').modal('show');
		$('.network-detect').fadeIn();
	}
}
//Show toast
function show_toast(html){
	$('#toast_div').html(html).addClass('show');
	// After 3 seconds, remove the show class from DIV
	setTimeout(function(){ $('#toast_div').removeClass('show'); }, 5000);
}
