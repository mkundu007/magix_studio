	//Save edits of the Canvases of current Projects
    function modProj(data,callback){ 
		$.ajax({
			url:'/project/modify/',
			type:'POST',data:data,
    		success:function(resp) { 
    			
    			callback('success'); 
    		},
    		error:function(req,stat,err) { 
    			
    			callback('failed');
    		},
    		headers:{"X-CSRFToken": $('input[name="csrfmiddlewaretoken"]').attr('value')}
		}); 
	}
    
    //Save the name of current Project if modified
    function updateTitle(){
    	$('#proj_title').attr('disabled',true); 
    	$('#proj_title').prev().find('i').toggleClass('fa-edit').toggleClass('fa-spin fa-refresh'); 
		modProj({id:$('#proj_title').data('project_id'), title:$('#proj_title').val(), action:'title'},function(){
			$('#proj_title').removeAttr('disabled'); $('#proj_title').prev().find('i').toggleClass('fa-edit').toggleClass('fa-spin fa-refresh');
		});
    }
    
    function updateJson(){
    	$('#saveprogress').attr('disabled',true); $('#saveprogress').find('span').last().text('Saving Work'); $('#saveprogress').find('i').toggleClass('fa-check').toggleClass('fa-spin fa-refresh'); 
		modProj({id:$('#proj_title').data('project_id'),json:JSON.stringify(tempJson),action:'json'},function(){
			$('#saveprogress').removeAttr('disabled'); $('#saveprogress').find('span').last().text('Save Progress'); $('#saveprogress').find('i').toggleClass('fa-check').toggleClass('fa-spin fa-refresh');
		});
		$(window).unbind('beforeunload');
    }
    
    $(document).ready(function(e) {
    	//setInterval(checkconnection,5000);
    	//sub-side bar height maintain function
    	$('.side-nav-container').each(function() { 
    		$(this).css("height", (parseInt($('.container-fluid').height()) + 480) + "px" );
    	});
    	
    	//Save edits of the Canvases of current Projects
    	$('#saveprogress').click(updateJson);
    	
    	//Creating a preview after complete sudden editing
    	$('#previewproject').click(function(){
    		var canvas_node1 = document.createElement('canvas');
			canvas_node1.id = "tempCanv";
			canvas_node1.height = 400;
			canvas_node1.width = 640;

			var prvCanv= new fabric.Canvas(canvas_node1);
			$('body').append(prvCanv.wrapperEl);
			$('#tempCanv').parent().hide();
			
			 $('<div style="\n\
					    position: absolute;\n\
					    height: 100%;\n\
					    width: 100%;\n\
					    background: rgba(55,55,55,0.5);\n\
					    z-index: 1000;\n\
					"></div>').insertBefore($('#wrapper'));

			letAnim(prvCanv,0,10000);
			
//    		var v=0;rtc=0,faIcon=new fabric.IText("\uF021", { left: 325, top: 190, fill: '#666', fontSize: 100, fontFamily: 'Fontawesome', originX: 'center', originY: 'center', });
//    		prevcanvas.clear(); prevcanvas.add(faIcon); faIcon.seletable=false; prevcanvas.renderAll(); 
//    		
//    		setInterval(function(){ faIcon.rotate(rtc); prevcanvas.renderAll(); rtc++ },10);
//    		    		
//    		$('#previewModal').modal("show");
//    		frames=[];
//    		$.each(tempJson.frames.data,function(i,j){
//	    		setTimeout(function(){ 
//	    			prevcanvas.loadFromJSON(JSON.stringify(j.fabric),
//	    					function(){ v++; prevcanvas.renderAll.bind(prevcanvas);
//	    								primfeat=[prevcanvas.item(0).scaleX,prevcanvas.item(0).scaleY,prevcanvas.item(0).left]; 
//	    					    		letAnim(prevcanvas,j.duration*1000);
//	    					    		if(v==tempJson.frames.data.length-1){
//	    					        		setTimeout(function(){ prevcanvas.loadFromJSON(JSON.stringify(tempJson.frames.data[0].fabric),
//	    					        				function(){ prevcanvas.renderAll.bind(prevcanvas); $.each(prevcanvas.getObjects(),function(j,k){ }); },
//	    					        				function(o,obj){ 
//	    					        			console.log('end'); obj.opacity=0; obj.selectable=false; obj.editable=false; 
//	    					        			}); },j.duration*2000);
//	    					        	}
//	    					},
//	    					function(o,obj){ obj.selectable=false; obj.editable=false; });
//		    		var perc=0,step=100/3000; setInterval(function(){ $('.previewSave').css('width',perc+'%'); perc=perc+step; },3000);
//	    		},j.duration*1000);
//    		});
    	});
    	
    	//Save the name of current Project if modified
    	$('#proj_title').blur(updateTitle);
    	$('#proj_title').keydown(function(e){ if(e.which==13){ updateTitle(); } });
    	
    	//onClick hiding .side-nav
    	$('.handler').click(function(){
        	$('.side-nav').hide();
        	$('.side-nav').eq($(this).data('id')).fadeIn();
        });
    	
    	//onClick hiding .popup-editor
    	$('.close-popup-editor').click(function(){
        	$('.popup-editor').fadeOut();
        	$(".text-style").each( function() { 
        		$(this).removeClass("btn-secondary"); 
        		$(this).removeClass("btn-info"); 
        	});
        	setTimeout(function(){ $('.quick-editor').fadeIn(); },1000);
        	canvas.clear();
        	$(".make-bg-image-sel").hide();
        	$(".make-video-sel").hide();
        	load_min_canvas();
        });
    	
    	//Mouse hovering 
    	$('.current-upload').find('img').mouseenter(function() { $(this).prev().addClass('active')});
    	$('.side-nav-activity').mouseleave(function() { $(this).removeClass('active')});
    	
    	//onClick file firing
    	$('.uploadsubmit').click(function(e){
    		e.preventDefault();
    		$(this).parents('form').find('input[type="file"]').click();
    	});
    	
    	//Form submition after browsing files
    	$('input[type="file"]').change(function(){
    		$(this).parents('form').find('.uploadsubmit').attr('disabled', true);
    		$(this).parents('form').find('.uploadsubmit').html('Uploading <i class="fas fa-refresh fa-spin fa-fw"></i>');
    		$('.progress').css("opacity", "1");
    		
    		file_upload($(this).parents('form'));
    	});
    	
    	//onClick file deletion modal call
    	$('.make-del').click(function(e){
    		$("#mediaDeletiontModal").find(".make-del-confirm").data('del',$(this).parent().data('del'));
        	$("#mediaDeletiontModal").modal("show");
        	
        });
    	
    	//onClick file delete call
    	$('.make-del-confirm').click(function(e){
        	file_del($(this));
        	
        });
    	
    	//onClick audio play function call
    	$('.make-audio-play').click(function(e){
        	e.preventDefault();
        	
        	audio_play($(this));
        	
        });
    	
    	//onClick video play function call
    	$('.make-video-play').click(function(e){
        	e.preventDefault();
        	
        	video_play($(this));
        	
        });
    	
    	/*==========================================================================================================================*/
    	/*==================================================< JS for For Sliders >==================================================*/
    	/*==========================================================================================================================*/
    	
    	let titleSize_slider = document.getElementById("titleSize");
    	let subtitleSize_slider = document.getElementById("subtitleSize");
    	let durationLength_slider = document.getElementById("durationLength");
    	let photoScale_slider = document.getElementById("photoScale");
    	
    	let titleSize_output = document.getElementById("titleSize-value");
    	let subtitleSize_output = document.getElementById("subtitleSize-value");
    	let durationLength_output = document.getElementById("durationLength-value");
    	
    	let photoScale_output = document.getElementById("photoScale-value");
    	
    	titleSize_output.innerHTML = titleSize_slider.value;
    	subtitleSize_output.innerHTML = subtitleSize_slider.value;
    	durationLength_output.innerHTML = durationLength_slider.value;
    	photoScale_output.innerHTML = photoScale_slider.value;

    	titleSize_slider.oninput = function() {
    		titleSize_output.innerHTML = this.value;
    	}
    	
    	subtitleSize_slider.oninput = function() {
    		subtitleSize_output.innerHTML = this.value;
    	}
    	
    	durationLength_slider.oninput = function() {
    		durationLength_output.innerHTML = this.value;
    	}
    	
    	photoScale_slider.oninput = function() {
    		photoScale_output.innerHTML = this.value;
    	}
    	
    	/*==========================================================================================================================*/
    	/*==============================================< JS for mini editor canvas >===============================================*/
    	/*==========================================================================================================================*/
    	
    	//Load mini canvas 
    	load_min_canvas();
    	
    	//Edit color of Title of every canvas at once
    	$('#wholeTitleColor').change(function() {
    		var value = $(this).val(); 
    		$.each(mini_canvas, function(i, j) { 
    			var ind = j.item(1).type =='rect' && $.isArray(tempJson.frames.data[i].media.url) ? tempJson.frames.data[i].media.url.length*2 : 1 ;
    			j.item(ind).set('fill', value); 
    			j.renderAll();
    			tempJson.frames.data[i].fabric.objects[ind].fill=value;
    			tempJson.frames.data[i].color.title=value;
    		});

    	});
    	
    	//Edit color of SubTitle of every canvas at once
    	$('#wholeSubTitleColor').change(function() {
    		var value = $(this).val(); 
    		$.each(mini_canvas, function(i, j) { 
    			var ind = j.item(1).type =='rect' && $.isArray(tempJson.frames.data[i].media.url) ? tempJson.frames.data[i].media.url.length*2 : 1 ;
    			j.item(ind+1).set('fill', value); 
    			j.renderAll(); 
    			tempJson.frames.data[i].fabric.objects[ind+1].fill=value;
    		});

    	});
    	
    	//Edit bg solid color of every canvas at once
    	$('#wholeBgColor').change(function() {
    		var value = $(this).val(); 
    		$.each(mini_canvas, function(i, j) { 
    			j.set('backgroundColor', value); 
    			j.renderAll(); 
    			tempJson.frames.data[i].color.background=value;
    		});
    		canvas.set('backgroundColor', value).renderAll();
    		tempJson.design.color.background=value;
   		});
    	
    	//Edit Font-Family of every canvas at once
    	$('.wholeFontFamily').change(function(){
    		var value = $(this).val();
    		$.each(mini_canvas, function(i, j) { 
    			var ind = j.item(1).type =='rect' && $.isArray(tempJson.frames.data[i].media.url) ? tempJson.frames.data[i].media.url.length*2 : 1 ;
    			j.item(ind).set('fontFamily', value); 
    			j.item(ind+1).set('fontFamily', value); 
    			j.renderAll(); 
    			tempJson.frames.data[i].fabric.objects[ind].fontFamily=value;
    			tempJson.frames.data[i].fabric.objects[ind+1].fontFamily=value;
    		});
    		
    		if (canvas._objects.length != 0) {
	    		var ind = canvas.item(1).type =='image' && $.isArray(tempJson.frames.data[i].media.url) ? tempJson.frames.data[i].media.url.length*2 : 1 ;
	    		canvas.item(ind).set('fontFamily', value); 
	    		canvas.item(ind+1).set('fontFamily', value); 
	    		canvas.renderAll(); 
	    		$.each(mini_canvas, function(i, j) { 
	    			var ind = j.item(1).type =='rect' && $.isArray(tempJson.frames.data[i].media.url) ? tempJson.frames.data[i].media.url.length*2 : 1 ;
	    			tempJson.frames.data[i].fabric.objects[ind].fontFamily=value;
	    			tempJson.frames.data[i].fabric.objects[ind+1].fontFamily=value;
	    		});
    		}
    		
    		tempJson.design.font=value;
    	});
    	
    	/*==========================================================================================================================*/
    	/*==============================================< JS for main editor canvas >===============================================*/
    	/*==========================================================================================================================*/
    	
    	//onClick image select or background select on canvas function call
    	$('.make-bg-image-sel').click(function(e){
        	
        	imagebackground_select($(this));
        });
    	
    	//onClick image select or background reset on canvas function call
    	$('.reset_background').click(function(e){
        
        	imagebackground_reset($(this));
        });
    	
    	//onClick watermark select on canvas function call
    	$('.make-watermark-sel').click(function(e){
    		
    		watermarkSizeChangeLimit = 1;
    		
        	watermark_select($(this));
        });
    	
    	//Edit duration per frame
    	$('#durationLength').bind('input propertychange', function(e) {
    		var clone = totalDuration;
    		clone[miniCanvasIndex] = $(this).val();
    		if ( eval(clone.join("+")) <= 90) {
	    		tempJson.frames.data[miniCanvasIndex].duration=$(this).val();
	    		
	    		//Count Total Duration of All Frames
			   	totalDuration[miniCanvasIndex] = tempJson.frames.data[miniCanvasIndex].duration;
			   	$("#totalduration").find('.text').html("<b>" + eval(totalDuration.join("+")) + " Sec </b>");
    		} else {
    			var target = e.target;
                target.value = target.value - 20;
                $("#durationLength-value").text("1");
                $("#durationLength").val(1);
                tempJson.frames.data[miniCanvasIndex].duration=1;
	    		
	    		//Count Total Duration of All Frames
			   	totalDuration[miniCanvasIndex] = tempJson.frames.data[miniCanvasIndex].duration;
			   	$("#totalduration").find('.text').html("<b>" + eval(totalDuration.join("+")) + " Sec </b>");
                e.preventDefault();
    			show_toast("The Duration Limit reached and try to reduce some.");
    		}
        });
        
    	//Edit content of Title
    	$('#titleinput').bind('input propertychange', function() {
			 var ind = tempJson.frames.data[miniCanvasIndex].type =='collage' && $.isArray(tempJson.frames.data[miniCanvasIndex].media.url) ? tempJson.frames.data[miniCanvasIndex].media.url.length*2 : 1 ;
    		 canvas.item(ind).set("text",$(this).val());
             canvas.renderAll();
             tempJson.frames.data[miniCanvasIndex].fabric.objects[ind].text=$(this).val();
    	});
    	
    	//Edit size of Title
    	$('#titleSize').bind('input propertychange', function() {
			 var ind = tempJson.frames.data[miniCanvasIndex].type =='collage' && $.isArray(tempJson.frames.data[miniCanvasIndex].media.url) ? tempJson.frames.data[miniCanvasIndex].media.url.length*2 : 1 ;
    		 canvas.item(ind).set("fontSize",$(this).val());
             canvas.renderAll();
             tempJson.frames.data[miniCanvasIndex].fabric.objects[ind].fontSize=$(this).val();
    	});
    	
    	//Edit color of Title
    	$('#titleColor').bind('input propertychange', function() {
			 var ind = tempJson.frames.data[miniCanvasIndex].type =='collage' && $.isArray(tempJson.frames.data[miniCanvasIndex].media.url) ? tempJson.frames.data[miniCanvasIndex].media.url.length*2 : 1 ;
    		 canvas.item(ind).set("fill",$(this).val());
             canvas.renderAll(); 
             tempJson.frames.data[miniCanvasIndex].fabric.objects[ind].fill=$(this).val();
    	});
    	
    	//Edit bgcolor of Title
    	$('#titleBgColor').bind('input propertychange', function() {
			 var ind = tempJson.frames.data[miniCanvasIndex].type =='collage' && $.isArray(tempJson.frames.data[miniCanvasIndex].media.url) ? tempJson.frames.data[miniCanvasIndex].media.url.length*2 : 1 ;
    		 canvas.item(ind).set("textBackgroundColor",$(this).val());
             canvas.renderAll();
             tempJson.frames.data[miniCanvasIndex].fabric.objects[ind].backgroundColor=$(this).val();
    	});
    	
    	//Edit content of Subtitle
    	$('#subtitleinput').bind('input propertychange', function() {
			var ind = tempJson.frames.data[miniCanvasIndex].type =='collage' && $.isArray(tempJson.frames.data[miniCanvasIndex].media.url) ? tempJson.frames.data[miniCanvasIndex].media.url.length*2 : 1 ;
   		 	canvas.item(ind+1).set("text",$(this).val());
            canvas.renderAll();
            tempJson.frames.data[miniCanvasIndex].fabric.objects[ind+1].text=$(this).val();
   		});
        
    	//Edit size of Subtitle
    	$('#subtitleSize').bind('input propertychange', function() {
			 var ind = tempJson.frames.data[miniCanvasIndex].type =='collage' && $.isArray(tempJson.frames.data[miniCanvasIndex].media.url) ? tempJson.frames.data[miniCanvasIndex].media.url.length*2 : 1 ;
    		 canvas.item(ind+1).set("fontSize",$(this).val());
             canvas.renderAll();
             tempJson.frames.data[miniCanvasIndex].fabric.objects[ind+1].fontSize=$(this).val();
    	});
    	
    	
    	//Edit color of Subtitle
    	$('#subtitleColor').bind('input propertychange', function() {
			 var ind = tempJson.frames.data[miniCanvasIndex].type =='collage' && $.isArray(tempJson.frames.data[miniCanvasIndex].media.url) ? tempJson.frames.data[miniCanvasIndex].media.url.length*2 : 1 ;
    		 canvas.item(ind+1).set("fill",$(this).val());
             canvas.renderAll();
             tempJson.frames.data[miniCanvasIndex].fabric.objects[ind+1].fill=$(this).val();
    	});
    	
    	//Edit bgColor of Subtitle
    	$('#subtitleBgColor').bind('input propertychange', function() {
			 var ind = tempJson.frames.data[miniCanvasIndex].type =='collage' && $.isArray(tempJson.frames.data[miniCanvasIndex].media.url) ? tempJson.frames.data[miniCanvasIndex].media.url.length*2 : 1 ;
    		 canvas.item(ind+1).set("textBackgroundColor",$(this).val());
             canvas.renderAll();
             tempJson.frames.data[miniCanvasIndex].fabric.objects[ind+1].backgroundColor=$(this).val();
    	});
    	
    	//Edit text style of title and Subtitle
    	$('.text-style').click(function() {
    		$(this).toggleClass("btn-secondary");
    		$(this).toggleClass("btn-info");
    		
    		var ind = tempJson.frames.data[miniCanvasIndex].type =='collage' && $.isArray(tempJson.frames.data[miniCanvasIndex].media.url) ? tempJson.frames.data[miniCanvasIndex].media.url.length*2 : 1 ;
    		if ($(this).hasClass("btn-info")) {
    			if ($(this).data('activity') == "title-bold") {
    				canvas.item(ind).set("fontWeight","bold");
    	            canvas.renderAll();
    	            tempJson.frames.data[miniCanvasIndex].fabric.objects[ind].fontWeight="bold";
    			} else if ($(this).data('activity') == "subtitle-bold") {
    				canvas.item(ind+1).set("fontWeight","bold");
    	            canvas.renderAll();
    	            tempJson.frames.data[miniCanvasIndex].fabric.objects[ind+1].fontWeight="bold";
    			} else if ($(this).data('activity') == "title-underline") {
    				canvas.item(ind).set("underline",true);
    	            canvas.renderAll();
    	            tempJson.frames.data[miniCanvasIndex].fabric.objects[ind].underline=true;
    			} else if ($(this).data('activity') == "subtitle-underline") {
    				canvas.item(ind+1).set("underline",true);
    	            canvas.renderAll();
    	            tempJson.frames.data[miniCanvasIndex].fabric.objects[ind+1].underline=true;
    			} else if ($(this).data('activity') == "title-italic") {
    				canvas.item(ind).set("fontStyle","italic");
    	            canvas.renderAll();
    	            tempJson.frames.data[miniCanvasIndex].fabric.objects[ind].fontStyle="italic";
    			} else {
    				canvas.item(ind+1).set("fontStyle","italic");
    	            canvas.renderAll();
    	            tempJson.frames.data[miniCanvasIndex].fabric.objects[ind+1].fontStyle="italic";
    			}
    		} else {
    			if ($(this).data('activity') == "title-bold") {
    				canvas.item(ind).set("fontWeight","normal");
    	            canvas.renderAll();
    	            tempJson.frames.data[miniCanvasIndex].fabric.objects[ind].fontWeight="normal";
    			} else if ($(this).data('activity') == "subtitle-bold") {
    				canvas.item(ind+1).set("fontWeight","normal");
    	            canvas.renderAll();
    	            tempJson.frames.data[miniCanvasIndex].fabric.objects[ind+1].fontWeight="normal";
    			} else if ($(this).data('activity') == "title-underline") {
    				canvas.item(ind).set("underline",false);
    	            canvas.renderAll();
    	            tempJson.frames.data[miniCanvasIndex].fabric.objects[ind].underline=false;
    			} else if ($(this).data('activity') == "subtitle-underline") {
    				canvas.item(ind+1).set("underline",false);
    	            canvas.renderAll();
    	            tempJson.frames.data[miniCanvasIndex].fabric.objects[ind+1].underline=false;
    			} else if ($(this).data('activity') == "title-italic") {
    				canvas.item(ind).set("fontStyle","normal");
    	            canvas.renderAll();
    	            tempJson.frames.data[miniCanvasIndex].fabric.objects[ind].fontStyle="normal";
    			} else {
    				canvas.item(ind+1).set("fontStyle","normal");
    	            canvas.renderAll();
    	            tempJson.frames.data[miniCanvasIndex].fabric.objects[ind+1].fontStyle="normal";
    			}
    		}
    	});
    	
    	//Edit size of Watermarks
    	$('.watermark-size').click(function() {
    		var x = mini_canvas[0].getObjects().length-1;
    		var watermarkScaleX = tempJson.frames.data[0].fabric.objects[x].scaleX, watermarkScaleY = tempJson.frames.data[0].fabric.objects[x].scaleY;
    		
    		//Increasing the size of Watermarks 
    		if ($(this).data('activity') == "watermark-large" && watermarkSizeChangeLimit < 2) {
    			
    			//Increasing the size of Watermarks in "top-left" position
    			if (tempJson.design.watermark.position == "top-left") {
	    			$.each(mini_canvas, function(i, j) { console.log(j._objects);
	    				var last_item_index = (j._objects.length - 1);
	    				var x=tempJson.frames.data[i].fabric.objects[last_item_index].scaleX+0.05;
	        			var y=tempJson.frames.data[i].fabric.objects[last_item_index].scaleY+0.05;
	        			var left=((j.item(last_item_index).width*x)/2)+20;
	        			var top=((j.item(last_item_index).height*y)/2)+20;
	    				var ratio = j.width/645;
	    		    	
	    				j.item(last_item_index).set({
	    					'scaleX': x*ratio,
	    			    	'scaleY': y*ratio,
	    			    	'left': left*ratio,
	    			    	'top': top*ratio
	    		    	});
	    				j.renderAll();
	    				tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=x;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=y;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].left=left;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].top=top;
	    		    });
	    				
	    			if (canvas._objects.length != 0) {
	    				var x=canvas.item(canvas._objects.length - 1).scaleX+0.05;
	        			var y=canvas.item(canvas._objects.length - 1).scaleY+0.05;
	        			var left=((canvas.item(canvas._objects.length - 1).width*x)/2)+20;
	        			var top=((canvas.item(canvas._objects.length - 1).height*y)/2)+20;
	    			    canvas.item(canvas._objects.length - 1).set({
	    			    	'scaleX': x,
	    			    	'scaleY': y,
	    			    	'left': left,
	    			    	'top': top
	    				});
	    			    canvas.renderAll();
	    				$.each(mini_canvas, function(i, j) { 
	    					var last_item_index = (j._objects.length - 1);
	    					tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=x;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=y;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].left=left;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].top=top;
	    				});
	    			} watermarkSizeChangeLimit+=0.5;
    			}
    			
    			//Increasing the size of Watermarks in "top-right" position
    			else if (tempJson.design.watermark.position == "top-right") {
    				$.each(mini_canvas, function(i, j) { 
	    				var last_item_index = (j._objects.length - 1);
	    				var x=tempJson.frames.data[i].fabric.objects[last_item_index].scaleX+0.05;
	        			var y=tempJson.frames.data[i].fabric.objects[last_item_index].scaleY+0.05;
	        			var left=((j.item(last_item_index).width*x)/2)+20;
	        			var top=((j.item(last_item_index).height*y)/2)+20;
	    				var ratio = j.width/645;
	    		    	
	    				j.item(last_item_index).set({
	    					'scaleX': x*ratio,
	    			    	'scaleY': y*ratio,
	    			    	'left': j.width-(left*ratio),
	    			    	'top': top*ratio
	    		    	});
	    				j.renderAll();
	    				tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=x;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=y;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].left=canvas.width-left;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].top=top;
	    		    });
	    				
	    			if (canvas._objects.length != 0) {
	    				var x=canvas.item(canvas._objects.length - 1).scaleX+0.05;
	        			var y=canvas.item(canvas._objects.length - 1).scaleY+0.05;
	        			var left=((canvas.item(canvas._objects.length - 1).width*x)/2)+20;
	        			var top=((canvas.item(canvas._objects.length - 1).height*y)/2)+20;
	    			    canvas.item(canvas._objects.length - 1).set({
	    			    	'scaleX': x,
	    			    	'scaleY': y,
	    			    	'left': canvas.width-left,
	    			    	'top': top
	    				});
	    			    canvas.renderAll();
	    				$.each(mini_canvas, function(i, j) { 
	    					var last_item_index = (j._objects.length - 1);
	    					tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=x;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=y;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].left=canvas.width-left;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].top=top;
	    				});
	    			} watermarkSizeChangeLimit+=0.5;
    			}
    			
    			//Increasing the size of Watermarks in "bottom-left" position
    			else if (tempJson.design.watermark.position == "bottom-left") {
    				$.each(mini_canvas, function(i, j) { 
	    				var last_item_index = (j._objects.length - 1);
	    				var x=tempJson.frames.data[i].fabric.objects[last_item_index].scaleX+0.05;
	        			var y=tempJson.frames.data[i].fabric.objects[last_item_index].scaleY+0.05;
	        			var left=((j.item(last_item_index).width*x)/2)+20;
	        			var top=((j.item(last_item_index).height*y)/2)+20;
	    				var ratio = j.width/645;
	    		    	
	    				j.item(last_item_index).set({
	    					'scaleX': x*ratio,
	    			    	'scaleY': y*ratio,
	    			    	'left': left*ratio,
	    			    	'top': j.height-(top*ratio)
	    		    	});
	    				j.renderAll();
	    				tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=x;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=y;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].left=left;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].top=canvas.height-top;
	    		    });
	    				
	    			if (canvas._objects.length != 0) {
	    				var x=canvas.item(canvas._objects.length - 1).scaleX+0.05;
	        			var y=canvas.item(canvas._objects.length - 1).scaleY+0.05;
	        			var left=((canvas.item(canvas._objects.length - 1).width*x)/2)+20;
	        			var top=((canvas.item(canvas._objects.length - 1).height*y)/2)+20;
	    			    canvas.item(canvas._objects.length - 1).set({
	    			    	'scaleX': x,
	    			    	'scaleY': y,
	    			    	'left': left,
	    			    	'top': canvas.height-top
	    				});
	    			    canvas.renderAll();
	    				$.each(mini_canvas, function(i, j) { 
	    					var last_item_index = (j._objects.length - 1);
	    					tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=x;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=y;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].left=left;
		    				tempJson.frames.data[i].fabric.objects[last_item_index].top=canvas.height-top;
	    				});
	    			} watermarkSizeChangeLimit+=0.5;
    			}
    			
    			//Increasing the size of Watermarks in "bottom-right" position
    			else {
    				$.each(mini_canvas, function(i, j) { 
	    				var last_item_index = (j._objects.length - 1);
	    				var x=tempJson.frames.data[i].fabric.objects[last_item_index].scaleX+0.05;
	        			var y=tempJson.frames.data[i].fabric.objects[last_item_index].scaleY+0.05;
	        			var left=((j.item(last_item_index).width*x)/2)+20;
	        			var top=((j.item(last_item_index).height*y)/2)+20;
	    				var ratio = j.width/645;
	    		    	
	    				j.item(last_item_index).set({
	    					'scaleX': x*ratio,
	    			    	'scaleY': y*ratio,
	    			    	'left': j.width-(left*ratio),
	    			    	'top': j.height-(top*ratio)
	    		    	});
	    				j.renderAll();
	    				tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=x;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=y;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].left=canvas.width-left;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].top=canvas.height-top;
	    		    });
	    				
	    			if (canvas._objects.length != 0) {
	    				var x=canvas.item(canvas._objects.length - 1).scaleX+0.05;
	        			var y=canvas.item(canvas._objects.length - 1).scaleY+0.05;
	        			var left=((canvas.item(canvas._objects.length - 1).width*x)/2)+20;
	        			var top=((canvas.item(canvas._objects.length - 1).height*y)/2)+20;
	    			    canvas.item(canvas._objects.length - 1).set({
	    			    	'scaleX': x,
	    			    	'scaleY': y,
	    			    	'left': canvas.width-left,
	    			    	'top': canvas.height-top
	    				});
	    			    canvas.renderAll();
	    				$.each(mini_canvas, function(i, j) { 
	    					var last_item_index = (j._objects.length - 1);
	    					tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=x;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=y;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].left=canvas.width-left;
		    				tempJson.frames.data[i].fabric.objects[last_item_index].top=canvas.height-top;
	    				});
	    			} watermarkSizeChangeLimit+=0.5;
    			}
    		} 
    		
    		//Decreasing the size of Watermarks
    		else if ($(this).data('activity') == "watermark-small" && watermarkScaleX > 0.1 && watermarkScaleY > 0.1 && watermarkSizeChangeLimit > 0.5) {
    			
    			//Decreasing the size of Watermarks in "top-left" position
    			if (tempJson.design.watermark.position == "top-left") {
	    			$.each(mini_canvas, function(i, j) { 
	    				var last_item_index = (j._objects.length - 1);
	    				var x=tempJson.frames.data[i].fabric.objects[last_item_index].scaleX-0.05;
	        			var y=tempJson.frames.data[i].fabric.objects[last_item_index].scaleY-0.05;
	        			var left=((j.item(last_item_index).width*x)/2)+20;
	        			var top=((j.item(last_item_index).height*y)/2)+20;
	    				var ratio = j.width/645;
	    		    	
	    				j.item(last_item_index).set({
	    					'scaleX': x*ratio,
	    			    	'scaleY': y*ratio,
	    			    	'left': left*ratio,
	    			    	'top': top*ratio
	    		    	});
	    				j.renderAll();
	    				tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=x;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=y;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].left=left;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].top=top;
	    		    });
	    				
	    			if (canvas._objects.length != 0) {
	    				var x=canvas.item(canvas._objects.length - 1).scaleX-0.05;
	        			var y=canvas.item(canvas._objects.length - 1).scaleY-0.05;
	        			var left=((canvas.item(canvas._objects.length - 1).width*x)/2)+20;
	        			var top=((canvas.item(canvas._objects.length - 1).height*y)/2)+20;
	    			    canvas.item(canvas._objects.length - 1).set({
	    			    	'scaleX': x,
	    			    	'scaleY': y,
	    			    	'left': left,
	    			    	'top': top
	    				});
	    			    canvas.renderAll();
	    				$.each(mini_canvas, function(i, j) { 
	    					var last_item_index = (j._objects.length - 1);
	    					tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=x;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=y;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].left=left;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].top=top;
	    				});
	    			} watermarkSizeChangeLimit-=0.5;
    			}
    			
    			//Decreasing the size of Watermarks in "top-right" position
    			else if (tempJson.design.watermark.position == "top-right") {
    				$.each(mini_canvas, function(i, j) { 
	    				var last_item_index = (j._objects.length - 1);
	    				var x=tempJson.frames.data[i].fabric.objects[last_item_index].scaleX-0.05;
	        			var y=tempJson.frames.data[i].fabric.objects[last_item_index].scaleY-0.05;
	        			var left=((j.item(last_item_index).width*x)/2)+20;
	        			var top=((j.item(last_item_index).height*y)/2)+20;
	    				var ratio = j.width/645;
	    		    	
	    				j.item(last_item_index).set({
	    					'scaleX': x*ratio,
	    			    	'scaleY': y*ratio,
	    			    	'left': j.width-(left*ratio),
	    			    	'top': top*ratio
	    		    	});
	    				j.renderAll();
	    				tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=x;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=y;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].left=canvas.width-left;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].top=top;
	    		    });
	    				
	    			if (canvas._objects.length != 0) {
	    				var x=canvas.item(canvas._objects.length - 1).scaleX-0.05;
	        			var y=canvas.item(canvas._objects.length - 1).scaleY-0.05;
	        			var left=((canvas.item(canvas._objects.length - 1).width*x)/2)+20;
	        			var top=((canvas.item(canvas._objects.length - 1).height*y)/2)+20;
	    			    canvas.item(canvas._objects.length - 1).set({
	    			    	'scaleX': x,
	    			    	'scaleY': y,
	    			    	'left': canvas.width-left,
	    			    	'top': top
	    				});
	    			    canvas.renderAll();
	    				$.each(mini_canvas, function(i, j) { 
	    					var last_item_index = (j._objects.length - 1);
	    					tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=x;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=y;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].left=canvas.width-left;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].top=top;
	    				});
	    			} watermarkSizeChangeLimit-=0.5;
    			}
    			
    			//Decreasing the size of Watermarks in "bottom-left" position
    			else if (tempJson.design.watermark.position == "bottom-left") {
    				$.each(mini_canvas, function(i, j) { 
	    				var last_item_index = (j._objects.length - 1);
	    				var x=tempJson.frames.data[i].fabric.objects[last_item_index].scaleX-0.05;
	        			var y=tempJson.frames.data[i].fabric.objects[last_item_index].scaleY-0.05;
	        			var left=((j.item(last_item_index).width*x)/2)+20;
	        			var top=((j.item(last_item_index).height*y)/2)+20;
	    				var ratio = j.width/645;
	    		    	
	    				j.item(last_item_index).set({
	    					'scaleX': x*ratio,
	    			    	'scaleY': y*ratio,
	    			    	'left': left*ratio,
	    			    	'top': j.height-(top*ratio)
	    		    	});
	    				j.renderAll();
	    				tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=x;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=y;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].left=left;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].top=canvas.height-top;
	    		    });
	    				
	    			if (canvas._objects.length != 0) {
	    				var x=canvas.item(canvas._objects.length - 1).scaleX-0.05;
	        			var y=canvas.item(canvas._objects.length - 1).scaleY-0.05;
	        			var left=((canvas.item(canvas._objects.length - 1).width*x)/2)+20;
	        			var top=((canvas.item(canvas._objects.length - 1).height*y)/2)+20;
	    			    canvas.item(canvas._objects.length - 1).set({
	    			    	'scaleX': x,
	    			    	'scaleY': y,
	    			    	'left': left,
	    			    	'top': canvas.height-top
	    				});
	    			    canvas.renderAll();
	    				$.each(mini_canvas, function(i, j) { 
	    					var last_item_index = (j._objects.length - 1);
	    					tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=x;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=y;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].left=left;
		    				tempJson.frames.data[i].fabric.objects[last_item_index].top=canvas.height-top;
	    				});
	    			} watermarkSizeChangeLimit-=0.5;
    			}
    			
    			//Decreasing the size of Watermarks in "bottom-right" position
    			else {
    				$.each(mini_canvas, function(i, j) { 
	    				var last_item_index = (j._objects.length - 1);
	    				var x=tempJson.frames.data[i].fabric.objects[last_item_index].scaleX-0.05;
	        			var y=tempJson.frames.data[i].fabric.objects[last_item_index].scaleY-0.05;
	        			var left=((j.item(last_item_index).width*x)/2)+20;
	        			var top=((j.item(last_item_index).height*y)/2)+20;
	    				var ratio = j.width/645;
	    		    	
	    				j.item(last_item_index).set({
	    					'scaleX': x*ratio,
	    			    	'scaleY': y*ratio,
	    			    	'left': j.width-(left*ratio),
	    			    	'top': j.height-(top*ratio)
	    		    	});
	    				j.renderAll();
	    				tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=x;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=y;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].left=canvas.width-left;
	    				tempJson.frames.data[i].fabric.objects[last_item_index].top=canvas.height-top;
	    		    });
	    				
	    			if (canvas._objects.length != 0) {
	    				var x=canvas.item(canvas._objects.length - 1).scaleX-0.05;
	        			var y=canvas.item(canvas._objects.length - 1).scaleY-0.05;
	        			var left=((canvas.item(canvas._objects.length - 1).width*x)/2)+20;
	        			var top=((canvas.item(canvas._objects.length - 1).height*y)/2)+20;
	    			    canvas.item(canvas._objects.length - 1).set({
	    			    	'scaleX': x,
	    			    	'scaleY': y,
	    			    	'left': canvas.width-left,
	    			    	'top': canvas.height-top
	    				});
	    			    canvas.renderAll();
	    				$.each(mini_canvas, function(i, j) { 
	    					var last_item_index = (j._objects.length - 1);
	    					tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=x;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=y;
	        				tempJson.frames.data[i].fabric.objects[last_item_index].left=canvas.width-left;
		    				tempJson.frames.data[i].fabric.objects[last_item_index].top=canvas.height-top;
	    				});
	    			} watermarkSizeChangeLimit-=0.5;
    			}
    			
    		}
    	});
    	
    	//Edit opacity of Watermarks
    	$('.watermark-opacity').click(function() {
    		$(this).toggleClass("btn-outline-primary");
    		$(this).toggleClass("btn-primary");
    		
    		//Decreasing opacity of Watermarks
    		if ($(this).hasClass("btn-primary")) {
    			$.each(mini_canvas, function(i, j) { 
    				last_item_index = (j._objects.length - 1);
    				ratio = j.width/645;
    		    		
    		    	j.item(last_item_index).set('opacity', 0.5 );
    				j.renderAll();
    				tempJson.frames.data[i].fabric.objects[last_item_index].opacity=0.5;
    		    });
    				
    			if (canvas._objects.length != 0) {
    			    canvas.item(canvas._objects.length - 1).set('opacity', 0.5 );
    				$.each(mini_canvas, function(i, j) { 
    					last_item_index = (j._objects.length - 1);
    					tempJson.frames.data[i].fabric.objects[last_item_index].opacity=0.5;
    				});
    			    canvas.renderAll();
    			}
	    	} 
    		
    		//Increasing opacity of Watermarks
    		else { 
	    		$.each(mini_canvas, function(i, j) { 
    				last_item_index = (j._objects.length - 1);
    				ratio = j.width/645;
    		    		
    		    	j.item(last_item_index).set('opacity', 1 );
    				j.renderAll();
    				tempJson.frames.data[i].fabric.objects[last_item_index].opacity=1;
    		    });
    				
    			if (canvas._objects.length != 0) {
    			    canvas.item(canvas._objects.length - 1).set('opacity', 1 );
    				$.each(mini_canvas, function(i, j) { 
    					last_item_index = (j._objects.length - 1);
    					tempJson.frames.data[i].fabric.objects[last_item_index].opacity=1;
    				});
    			    canvas.renderAll();
    			}
	    	} 
    	});
    	
    	//Edit positions of Watermarks
    	$('.watermark-position').click(function() {
    		
    		//Set watermarks in "top-left" position
    		if($(this).data('position') == "top-left"){
    			watermarkSizeChangeLimit = 1;
    			tempJson.design.watermark.position="top-left";
    			
    			var canvas_ratio, aspect_ratio, actualheight, actualwidth, targetheight, targetwidth;
    			$.each(mini_canvas, function(i, j) { 
    				var last_item_index = (j._objects.length - 1);
    				
    	    		canvas_ratio = j.width/645;
    	    		aspect_ratio = j.item(last_item_index).width/j.item(last_item_index).height;
    		    	actualheight = j.item(last_item_index).height;
    		    	targetheight = 50;
    		    	actualwidth = j.item(last_item_index).width;
    		    	targetwidth = (targetheight * aspect_ratio);
    	    		j.item(last_item_index).set({
    	    			'scaleX': (targetwidth/actualwidth)*canvas_ratio,
    	    			'scaleY': (targetheight/actualheight)*canvas_ratio,
    	    		    'left': ((targetwidth/2)+20)*canvas_ratio,
    	    		    'top': ((targetheight/2)+20)*canvas_ratio
    	    		});
    	    		j.renderAll();
    	    		tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=(targetwidth/actualwidth);
        		    tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=(targetheight/actualheight);
    	    		tempJson.frames.data[i].fabric.objects[last_item_index].top=(targetheight/2)+20;
    	    		tempJson.frames.data[i].fabric.objects[last_item_index].left=(targetwidth/2)+20;	    
    	    	});
    			
    			if (canvas._objects.length != 0) {
    	    		aspect_ratio = canvas.item(canvas._objects.length - 1).width/canvas.item(canvas._objects.length - 1).height;
    		    	actualheight = canvas.item(canvas._objects.length - 1).height;
    		    	targetheight = 50;
    		    	actualwidth = canvas.item(canvas._objects.length - 1).width;
    		    	targetwidth = (targetheight * aspect_ratio);
    	    		canvas.item(canvas._objects.length - 1).set({
    	    			'scaleX': (targetwidth/actualwidth),
    	    			'scaleY': (targetheight/actualheight),
    	    		    'left': (targetwidth/2)+20,
    	    		    'top': (targetheight/2)+20
    	    		});
    	    		canvas.renderAll(); 
    	    		$.each(mini_canvas, function(i, j) { 
    	    			var last_item_index = (j._objects.length - 1);
    	    			tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=(targetwidth/actualwidth);
    	    		    tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=(targetheight/actualheight);
    	    			tempJson.frames.data[i].fabric.objects[last_item_index].top=(targetheight/2)+20;
    	    			tempJson.frames.data[i].fabric.objects[last_item_index].left=(targetwidth/2)+20;
    	    		});
    			}
    		} 
    		
    		//Set watermarks in "top-right" position
    		else if($(this).data('position') == "top-right"){
    			watermarkSizeChangeLimit = 1;
    			tempJson.design.watermark.position="top-right";
    			
    			var canvas_ratio, aspect_ratio, actualheight, actualwidth, targetheight, targetwidth;
    			$.each(mini_canvas, function(i, j) { 
    				var last_item_index = (j._objects.length - 1);
    				
    	    		canvas_ratio = j.width/645;
    	    		aspect_ratio = j.item(last_item_index).width/j.item(last_item_index).height;
    		    	actualheight = j.item(last_item_index).height;
    		    	targetheight = 50;
    		    	actualwidth = j.item(last_item_index).width;
    		    	targetwidth = (targetheight * aspect_ratio);
    	    		j.item(last_item_index).set({
    	    			'scaleX': (targetwidth/actualwidth)*canvas_ratio,
    	    			'scaleY': (targetheight/actualheight)*canvas_ratio,
    	    		    'left': j.width-(((targetwidth/2)+20)*canvas_ratio),
    	    		    'top': ((targetheight/2)+20)*canvas_ratio
    	    		});
    	    		j.renderAll();
    	    		tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=(targetwidth/actualwidth);
        		    tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=(targetheight/actualheight);
    	    		tempJson.frames.data[i].fabric.objects[last_item_index].top=(targetheight/2)+20;
    	    		tempJson.frames.data[i].fabric.objects[last_item_index].left=canvas.width-((targetwidth/2)+20);	    
    	    	});
    			
    			if (canvas._objects.length != 0) {
    				aspect_ratio = canvas.item(canvas._objects.length - 1).width/canvas.item(canvas._objects.length - 1).height;
    		    	actualheight = canvas.item(canvas._objects.length - 1).height;
    		    	targetheight = 50;
    		    	actualwidth = canvas.item(canvas._objects.length - 1).width;
    		    	targetwidth = (targetheight * aspect_ratio);
    	    		canvas.item(canvas._objects.length - 1).set({
    	    			'scaleX': (targetwidth/actualwidth),
    	    			'scaleY': (targetheight/actualheight),
    	    		    'left': canvas.width-((targetwidth/2)+20),
    	    		    'top': (targetheight/2)+20
    	    		});
    	    		canvas.renderAll(); 
    	    		$.each(mini_canvas, function(i, j) { 
    	    			var last_item_index = (j._objects.length - 1);
    	    			tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=(targetwidth/actualwidth);
    	    		    tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=(targetheight/actualheight);
    	    			tempJson.frames.data[i].fabric.objects[last_item_index].top=(targetheight/2)+20;
    	    			tempJson.frames.data[i].fabric.objects[last_item_index].left=canvas.width-((targetwidth/2)+20);
    	    		});
    			}
    		} 
    		
    		//Set watermarks in "bottom-left" position
    		else if($(this).data('position') == "bottom-left"){
    			watermarkSizeChangeLimit = 1;
    			tempJson.design.watermark.position="bottom-left";
    			
    			var canvas_ratio, aspect_ratio, actualheight, actualwidth, targetheight, targetwidth;
    			$.each(mini_canvas, function(i, j) { 
    				var last_item_index = (j._objects.length - 1);
    				
    	    		canvas_ratio = j.width/645;
    	    		aspect_ratio =j.item(last_item_index).width/j.item(last_item_index).height;
    		    	actualheight = j.item(last_item_index).height;
    		    	targetheight = 50;
    		    	actualwidth = j.item(last_item_index).width;
    		    	targetwidth = (targetheight * aspect_ratio);
    	    		j.item(last_item_index).set({
    	    			'scaleX': (targetwidth/actualwidth)*canvas_ratio,
    	    			'scaleY': (targetheight/actualheight)*canvas_ratio,
    	    		    'left': ((targetwidth/2)+20)*canvas_ratio,
    	    		    'top': j.height-(((targetheight/2)+20)*canvas_ratio)
    	    		});
    	    		j.renderAll();
    	    		tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=(targetwidth/actualwidth);
        		    tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=(targetheight/actualheight);
	    			tempJson.frames.data[i].fabric.objects[last_item_index].top=canvas.height-((targetheight/2)+20);
	    			tempJson.frames.data[i].fabric.objects[last_item_index].left=(targetwidth/2)+20;	    
    	    	});
    			
    			if (canvas._objects.length != 0) {
    				aspect_ratio = canvas.item(canvas._objects.length - 1).width/canvas.item(canvas._objects.length - 1).height;
    		    	actualheight = canvas.item(canvas._objects.length - 1).height;
    		    	targetheight = 50;
    		    	actualwidth = canvas.item(canvas._objects.length - 1).width;
    		    	targetwidth = (targetheight * aspect_ratio);
    	    		canvas.item(canvas._objects.length - 1).set({
    	    			'scaleX': (targetwidth/actualwidth),
    	    			'scaleY': (targetheight/actualheight),
    	    		    'left': (targetwidth/2)+20,
    	    		    'top': canvas.height-((targetheight/2)+20)
    	    		});
    	    		canvas.renderAll(); 
    	    		$.each(mini_canvas, function(i, j) { 
    	    			var last_item_index = (j._objects.length - 1);
    	    			tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=(targetwidth/actualwidth);
    	    		    tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=(targetheight/actualheight);
    	    			tempJson.frames.data[i].fabric.objects[last_item_index].top=canvas.height-((targetheight/2)+20);
    	    			tempJson.frames.data[i].fabric.objects[last_item_index].left=(targetwidth/2)+20;
    	    		});
    			}
    		} 
    		
    		//Set watermarks in "bottom-right" position
    		else {
    			watermarkSizeChangeLimit = 1;
    			tempJson.design.watermark.position="bottom-right";
    			
    			var canvas_ratio, aspect_ratio, actualheight, actualwidth, targetheight, targetwidth;
    			$.each(mini_canvas, function(i, j) { 
    				var last_item_index = (j._objects.length - 1);
    				
    	    		canvas_ratio = j.width/645;
    	    		aspect_ratio =j.item(last_item_index).width/j.item(last_item_index).height;
    		    	actualheight = j.item(last_item_index).height;
    		    	targetheight = 50;
    		    	actualwidth = j.item(last_item_index).width;
    		    	targetwidth = (targetheight * aspect_ratio);
    	    		j.item(last_item_index).set({
    	    			'scaleX': (targetwidth/actualwidth)*canvas_ratio,
    	    			'scaleY': (targetheight/actualheight)*canvas_ratio,
    	    		    'left': j.width-(((targetwidth/2)+20)*canvas_ratio),
    	    		    'top': j.height-(((targetheight/2)+20)*canvas_ratio)
    	    		});
    	    		j.renderAll();
    	    		tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=(targetwidth/actualwidth);
        		    tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=(targetheight/actualheight);
	    			tempJson.frames.data[i].fabric.objects[last_item_index].top=canvas.height-((targetheight/2)+20);
	    			tempJson.frames.data[i].fabric.objects[last_item_index].left=canvas.width-((targetwidth/2)+20);	    
    	    	});
    			
    			if (canvas._objects.length != 0) {
    				aspect_ratio = canvas.item(canvas._objects.length - 1).width/canvas.item(canvas._objects.length - 1).height;
    		    	actualheight = canvas.item(canvas._objects.length - 1).height;
    		    	targetheight = 50;
    		    	actualwidth = canvas.item(canvas._objects.length - 1).width;
    		    	targetwidth = (targetheight * aspect_ratio);
    	    		canvas.item(canvas._objects.length - 1).set({
    	    			'scaleX': (targetwidth/actualwidth),
    	    			'scaleY': (targetheight/actualheight),
    	    		    'left': canvas.width-((targetwidth/2)+20),
    	    		    'top': canvas.height-((targetheight/2)+20)
    	    		});
    	    		canvas.renderAll(); 
    	    		$.each(mini_canvas, function(i, j) { 
    	    			var last_item_index = (j._objects.length - 1);
    	    			tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=(targetwidth/actualwidth);
    	    		    tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=(targetheight/actualheight);
    	    			tempJson.frames.data[i].fabric.objects[last_item_index].top=canvas.height-((targetheight/2)+20);
    	    			tempJson.frames.data[i].fabric.objects[last_item_index].left=canvas.width-((targetwidth/2)+20);
    	    		});
    			}
    		}
    	});
    	
    	//Clock-wise image rotations
    	$('#rotate-cw').click('input propertychange', function() {
    		 canvas.item(0).rotate(scaleIndex+=90);
             canvas.renderAll();
             tempJson.frames.data[miniCanvasIndex].fabric.objects[0].angle=scaleIndex;
    	});
    	
    	//Anti clock-wise image rotations
    	$('#rotate-acw').click('input propertychange', function() {
    		 canvas.item(0).rotate(scaleIndex-=90);
             canvas.renderAll();
             tempJson.frames.data[miniCanvasIndex].fabric.objects[0].angle=scaleIndex;
    	});
    	
    	//Image scaling
    	$('#photoScale').click('input propertychange', function() {
   		 	canvas.item(0).set({
   		 		"scaleX": $(this).val(),
   		 		"scaleY": $(this).val(),
   		 	});
            canvas.renderAll();
            tempJson.frames.data[miniCanvasIndex].fabric.objects[0].scaleX=$(this).val();
            tempJson.frames.data[miniCanvasIndex].fabric.objects[0].scaleY=$(this).val();
            
   		});
    	
    	//Edit solid bgColor
    	$('#bgColor').change(function() {
   		 	canvas.set("backgroundColor", $(this).val());
    		canvas.renderAll();
    		tempJson.frames.data[miniCanvasIndex].color.background=$(this).val();
   		});

        //onClick call to Video Trimmer appearance
    	$('.make-video-sel').click(function(e){
        	
        	video_trim($(this));
        	
        });
    	
    	//onClick call to Video Trimmer appearance for already exist video
    	$('.video-trimmer').click(function(e){
        	
        	video_trim($(this));
        	
        });
    	
    	//onClick call to Audio Trimmer appearance
    	$('.make-audio-sel').click(function(e){
        	
        	audio_trim($(this));
        	
        });
    	
    	//onClick play particular Video Frames
    	$('#playFrame').click(function(){ 
			letAnim(canvas,$('#durationLength').val()*1000); //tempJson.frames.data[miniCanvasIndex].duration); 
		});
    	
    	//onClick call to Frame Addition Modal
    	$('#frame_addition').click(function() {
    		$('#frameAdditionModal').modal("show");
    	});
    	
    	//Frame Addition function
    	$('.add_frame').click(function() {
    		var x = mini_canvas[0].getObjects().length-1;
    		if ( eval(totalDuration.join("+")) <= 90 ) {
	    		var watermark = JSON.stringify(tempJson.frames.data[0].fabric.objects[x]);
	    		if ($(this).data('action') == "add_photo") {
	    			if ((eval(totalDuration.join("+")) + 3) <= 90 ) {
		    			tempJson.frames.data.push(JSON.parse(readTextFile(mediaRoot+'frames/image.json').replace(/\{%GCBUCKET%}/g,mediaRoot).replace('{%watermark%}',watermark)));
		    			load_min_canvas();
	    			} else {
	    				show_toast("The Duration Limit reached and try to reduce some.");
	    			}
	    		} else if ($(this).data('action') == "add_text") {
	    			if ((eval(totalDuration.join("+")) + 5) <= 90 ) {
		    			tempJson.frames.data.push(JSON.parse(readTextFile(mediaRoot+'frames/text.json').replace(/\{%GCBUCKET%}/g,mediaRoot).replace('{%watermark%}',watermark)));
		    			load_min_canvas();
	    			} else {
	    				show_toast("The Duration Limit reached and try to reduce some.");
	    			}
	    		} else if ($(this).data('action') == "add_collage") {
	    			if ((eval(totalDuration.join("+")) + 10) <= 90 ) {
		    			tempJson.frames.data.push(JSON.parse(readTextFile(mediaRoot+'frames/collage.json').replace(/\{%GCBUCKET%}/g,mediaRoot).replace('{%watermark%}',watermark)));
		    			load_min_canvas();
	    			} else {
	    				show_toast("The Duration Limit reached and try to reduce some.");
	    			}
	    		} else if ($(this).data('action') == "add_logo") {
	    			if ((eval(totalDuration.join("+")) + 5) <= 90 ) {
		    			tempJson.frames.data.push(JSON.parse(readTextFile(mediaRoot+'frames/logo.json').replace(/\{%GCBUCKET%}/g,mediaRoot).replace('{%watermark%}',watermark)));
		    			load_min_canvas();
	    			} else {
	    				show_toast("The Duration Limit reached and try to reduce some.");
	    			}
	    		} else if ($(this).data('action') == "add_video") {
	    			if ((eval(totalDuration.join("+")) + 3) <= 37 ) {
		    			tempJson.frames.data.push(JSON.parse(readTextFile(mediaRoot+'frames/video.json').replace(/\{%GCBUCKET%}/g,mediaRoot).replace('{%watermark%}',watermark)));
		    			load_min_canvas();
	    			} else {
	    				show_toast("The Duration Limit reached and try to reduce some.");
	    			}
	    		}
    		} else {
    			show_toast("The Duration Limit reached and try to reduce some.");
    		}
    		$('#frameAdditionModal').modal("hide");
    	});
    	
    	//Frame Deletion Function
    	$('.delete-frame-confirm').click(function() { 
    		var x = $(this).data('id');
	    	tempJson.frames.data.splice(x, 1);
	    	totalDuration.splice(x, 1);
    		
    		$('#frameDeletiontModal').modal('hide');
    		
    		if ( $(this).data('callfrom') == "inside" ) { $('.close-popup-editor').click(); } else { load_min_canvas(); }
    	});
    	
    	//Rearranging the Frames
    	$('.quick-editor').find('.row').sortable({
    		cancel : '.new_frame',
    		update: function(event, ui) {
    					var new_index = $('.quick-editor').find('.row').children().index(ui.item);
    					var old_index = $('.quick-editor').find('.card').eq(new_index).find('.frame').data('id');
    					
    					if (new_index > old_index) {
    						tempJson.frames.data.splice(new_index+1, 0, tempJson.frames.data[old_index]);
    						tempJson.frames.data.splice(old_index, 1);
    					} else {
    						tempJson.frames.data.splice(new_index, 0, tempJson.frames.data[old_index]);
    						tempJson.frames.data.splice(old_index+1, 1);
    					}
    					load_min_canvas();
				}
    	});

    	//Div Collapse Controller
    	$('.lhs').click(function() {
    		$(this).parents('.design').find('.collapse').removeClass('show');
    		$(this).parents('.design').find('.lhs').addClass('collapsed');
    	});
    	
    	//Div Collapse Controller
    	$('.rhs').click(function() {
    		$(this).parents('.editor').find('.collapse').removeClass('show');
    		$(this).parents('.editor').find('.rhs').addClass('collapsed');
    	});
    	
    	//Duplicate Function of Image Upload
    	$('.uploadsubmit_duplicate').click(function() {
    		$('.side-nav').hide();
    		$('.side-nav').eq(0).show();
    		$('.side-nav').eq(0).find('.uploadsubmit').addClass("load_it");
    		$('.side-nav').eq(0).find('.uploadsubmit').click();
    	});
    	
    	setInterval(checkconnection, 3000);
    });  
	
	 
	//web-page before unload monitor function
	$(window).on('beforeunload', function(){
		confirm = confirm();
		if(confirm){
		    return true;
		} else {
			return false;
		}
	});

	//change grid to maintain canvas
	$(window).on('resize', function(){
		if($(window).width()<1560){
			$('.row.ui-sortable .col-md-4').each(function(){ $(this).toggleClass('col-md-6').toggleClass('col-md-4'); });
			$('li.nav-item.save-progress-btn span').each(function(){ $(this).css({'padding':'3px'}); });
			$('li.nav-item.save-progress-btn i').each(function(){ $(this).css({'font-size':'12px','padding':'0px 2px'}); });
			$('#totalduration b').text($('#totalduration b').text().substr(0,4));
			$('#saveprogress .text').text('Save');

			if($(window).width()<1230){ $('li.nav-item.save-progress-btn span.icon').hide(); }
			
		}else{
			$('.row.ui-sortable .col-md-6').each(function(){ $(this).toggleClass('col-md-6').toggleClass('col-md-4'); });
			$('li.nav-item.save-progress-btn span').each(function(){ $(this).removeAttr('style'); });
			$('li.nav-item.save-progress-btn i').each(function(){ $(this).removeAttr('style'); });
			$('#totalduration b').text($('#totalduration b').text().substr(0,4)+'ec');
			$('#saveprogress .text').text('Save Progress');
		}
	});
	
	function readTextFile(file)
	{
	    var rawFile = new XMLHttpRequest(),allText;
	    rawFile.open("GET", file, false);
	    rawFile.onreadystatechange = function ()
	    {
	        if(rawFile.readyState === 4)
	        {
	            if(rawFile.status === 200 || rawFile.status == 0)
	            {
	                allText = rawFile.responseText;
	            }
	        }
	    }
	    rawFile.send(null);
	    console.log(allText);
	    return allText;
	}