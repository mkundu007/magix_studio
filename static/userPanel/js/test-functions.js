
	function testAnim(){		
		canvas.item(0).set({scaleX:primfeat[0],scaleY:primfeat[1],left:primfeat[2]}); canvas.renderAll();		
	 	var circle0 = new fabric.Circle({		
	         radius: 400,		
	         left: 320,		
	         top: 200,		
	         hasControls: false,		
	         hasBorders: false,		
	         originX: "center",		
	         originY: "center",		
	         fill: "#fff",		
	     }); 		
	 	var circle = new fabric.Circle({		
	         radius: 100,		
	         left: 180,		
	         top: 205,		
	         hasControls: false,		
	         hasBorders: false,		
	         fill: "transparent",		
				stroke:'#456',		
				strokeWidth:30,		
	     }); 		
	 	var circle1 = new fabric.Circle({		
	         radius: 30,		
	         left: 435,		
	         top: 170,		
	         hasControls: false,		
	         hasBorders: false,		
	         fill: "transparent",		
				stroke:'#FF8C42',		
				strokeWidth:20,		
	     }); 		
	 	var circle2 = new fabric.Circle({		
	         radius: 20,		
	         left: 135,		
	         top: 24,		
	         hasControls: false,		
	         hasBorders: false,		
	         fill: "transparent",		
				stroke:'#6C8EAD',		
				strokeWidth:15,		
	     }); 		
	 	var circle3 = new fabric.Circle({		
	         radius: 80,		
	         left: 284,		
	         top: 13,		
	         hasControls: false,		
	         hasBorders: false,		
	         fill: "transparent",		
				stroke:'#A23E48',		
				strokeWidth:30,		
	     }); 		
	 	var circle4 = new fabric.Circle({		
	         radius: 70,		
	         left: 35,		
	         top: 116,		
	         hasControls: false,		
	         hasBorders: false,		
	         fill: "transparent",		
				stroke:'#FF3C38',		
				strokeWidth:25,		
	     }); 		
	 	canvas.add(circle0); console.log('added');		
	 	canvas.add(circle); console.log('added');		
	 	canvas.add(circle1); console.log('added');		
	 	canvas.add(circle2); console.log('added');		
	 	canvas.add(circle3); console.log('added');		
	 	canvas.add(circle4); console.log('added');		
		canvas.renderAll();		
	    			
	    	canvas.item(0).animate({scaleX:1.1,scaleY:1.1,left:350}, {		
	    	      duration: 1000,		
	    	      onChange: canvas.renderAll.bind(canvas),		
	    	      easing: fabric.util.ease[6]		
	    	    });		
	    			
	    	circle.animate({scaleX:0.5,scaleY:0.5}, {		
	    	      duration: 400,		
	    	      onChange: function(){ canvas.renderAll.bind(canvas); },		
	    	      easing: fabric.util.ease[6],		
	    	      onComplete: function(){		
	    	    	  circle.animate({scaleX:2,scaleY:2,opacity:0}, {		
	    	    	      duration: 600,		
	    	    	      onChange: function(){ canvas.renderAll.bind(canvas); },		
	    	    	      onComplete: function(){ canvas.remove(circle); canvas.renderAll(); },		
	    	    	      easing: fabric.util.ease[6]		
	    	    	    });		
	    	      }		
	    	    });		
	    	
	    	circle1.animate({scaleX:3,scaleY:3}, {		
	    	      duration: 800,		
	    	      onChange: function(){ canvas.renderAll.bind(canvas); },		
	    	      easing: fabric.util.ease[6],		
	    	      onComplete: function(){		
	    	    	  circle1.animate({scaleX:1,scaleY:1,opacity:0}, {		
	    	    	      duration: 700,		
	    	    	      onChange: function(){ canvas.renderAll.bind(canvas); },//console.log(canvas.toDataURL('png'));		
	    	    	      onComplete: function(){ canvas.remove(circle); canvas.renderAll(); },		
	    	    	      easing: fabric.util.ease[6]		
	    	    	    });		
	    	      }		
	    	    });		
	    			
	    	circle2.animate({scaleX:2.7,scaleY:2.7}, {		
	    	      duration: 700,		
	    	      onChange: function(){ canvas.renderAll.bind(canvas); },		
	    	      easing: fabric.util.ease[6],		
	    	      onComplete: function(){		
	    	    	  circle2.animate({scaleX:1,scaleY:1,opacity:0}, {		
	    	    	      duration: 600,		
	    	    	      onChange: function(){ canvas.renderAll.bind(canvas); },		
	    	    	      onComplete: function(){ canvas.remove(circle2); canvas.renderAll(); },		
	    	    	      easing: fabric.util.ease[6]		
	    	    	    });		
	    	      }		
	    	    });		
	    	circle3.animate({scaleX:0.7,scaleY:0.7}, {		
	    	      duration: 400,		
	    	      onChange: function(){ canvas.renderAll.bind(canvas); },		
	    	      easing: fabric.util.ease[6],		
	    	      onComplete: function(){		
	    	    	  circle3.animate({scaleX:1.9,scaleY:1.9,opacity:0}, {		
	    	    	      duration: 500,		
	    	    	      onChange: function(){ canvas.renderAll.bind(canvas); },		
	    	    	      onComplete: function(){ canvas.remove(circle3); canvas.renderAll(); },		
	    	    	      easing: fabric.util.ease[6]		
	    	    	    });		
	    	      }		
	    	    });		
	    			
	    	circle4.animate({scaleX:0.8,scaleY:0.8}, {		
	    	      duration: 400,		
	    	      onChange: function(){ canvas.renderAll.bind(canvas); },		
	    	      easing: fabric.util.ease[6],		
	    	      onComplete: function(){		
	    	    	  circle4.animate({scaleX:1.8,scaleY:1.8,opacity:0}, {		
	    	    	      duration: 500,		
	    	    	      onChange: function(){ canvas.renderAll.bind(canvas); },		
	    	    	      onComplete: function(){ canvas.remove(circle4); canvas.renderAll(); },		
	    	    	      easing: fabric.util.ease[6]		
	    	    	    });		
	    	      }		
	    	    });		
	    			
	    	circle0.animate({left:1000,top:700}, {		
		    	      duration: 1000,		
		    	      onChange: function(){ canvas.renderAll.bind(canvas); },		
 	    	      onComplete: function(){ canvas.remove(circle0); canvas.renderAll(); },		
		    	      easing: fabric.util.ease[6]		
		    	    });		
	 }		
	 		
	 function getCircles(color){		
	 	var circle = new fabric.Circle({		
	         radius: 500,		
	         left: 320,		
	         top: 200,		
	         hasControls: false,		
	         hasBorders: false,		
	         originX: "center",		
	         originY: "center",		
	         fill: color		
	     }); 		
	 	return circle;		
	 }		
	 		
	 function getRects(x,color){		
	 	var rect = new fabric.Rect({		
	 	    width: 128,		
	 	    height: 400,		
	 	    left: x,		
	 	    top: 0,		
	 	    fill: color,		
	 	    selectable: false		
	 	  });		
	 	return rect;		
	 }		
	 		
	 function getRects1(x,color){		
	 	var rect = new fabric.Rect({		
	 	    width: 145,		
	 	    height: 700,		
	 	    angle: 30,		
	 	    left: x,		
	 	    top: -100,		
	 	    fill: color,		
	 	    selectable: true		
	 	  });		
	 	return rect;		
	 }		
	 		
	 function animCircle(circle,callback=null){		
	 	circle.animate({radius:0}, {		
	 	      duration: 500,		
	 	      onChange: canvas.renderAll.bind(canvas),		
	 	      onComplete: function(){ canvas.remove(circle); if(callback){ callback(); } },		
	 	      easing: fabric.util.ease[6]		
	 	    });		
	 }		
	 
	 function animateBG(canv,duration,ind){
		 console.log(canv.getObjects());
		   canv.item(0).animate({scaleX:duration/500,scaleY:duration/500,left:duration/10}, {		
		      duration: duration,		
		      onChange: function(){ canv.renderAll.bind(canv); frames.push(canv.toDataURL('png')); console.log('working'); },	
		      onComplete: function(){ 
							canv.item(0).set({scaleX:primfeat[0],scaleY:primfeat[1],left:primfeat[2]});
							canv.renderAll();
							console.log('done');
							submitFrames(canv,ind);
		      			  },
		      easing: fabric.util.ease[6]		
		    });		
	 }
	 		
	 function animStrips(canv,i,strip,len,duration,ind){		
	 	var arr=i%2==0?{top:-400,opacity:0.7}:{top:400,opacity:0.7};	
	 	strip.animate(arr, {		
	 	      duration: 1000,		
		      onChange: function(){ canv.renderAll.bind(canv); frames.push(canv.toDataURL('png')); console.log('working'); },	
	 	      onComplete: function(){ canv.remove(strip); canv.renderAll(); if(i==len-1){ animateBG(canv,duration,ind); } },		
	 	      easing: fabric.util.ease[6]		
	 	    });		
	 }		
	 		
	 function animStrips1(i,strip){		
		   var mark1=-100,mark2=165;		
		   setInterval(function(){ 		
			   canvas.item(4).top=mark1; canvas.item(4).left=mark2; 		
			   mark1=mark1+10; mark2=mark2-5; canvas.renderAll(); },500);		
		   		
	 	var arr=i%2==0?{top:-400,left:-200,opacity:0.7}:{top:400,left:200,opacity:0.7};		
	 	strip.animate(arr, {		
	 	      duration: 1000,		
	 	      onChange: canvas.renderAll.bind(canvas),		
	 	      onComplete: function(){ canvas.remove(strip); },		
	 	      easing: fabric.util.ease[6]		
	 	    });		
	 }		
	 		
	//  function letAnim(){		
	//  	canvas.item(0).set({scaleX:primfeat[0],scaleY:primfeat[1],left:primfeat[2]}); canvas.renderAll();		
	//  	var circle1=getCircles('#aaa'),circle2=getCircles('#fff');		
	//  	canvas.add(circle1); canvas.add(circle2);		
	//  	canvas.item(0).animate({scaleX:1.1,scaleY:1.1,left:350}, {		
	//  	      duration: 2000,		
	//  	      onChange: canvas.renderAll.bind(canvas),		
	//  	      easing: fabric.util.ease[6]		
	//  	    });		
	//  	animCircle(circle2,animCircle(circle1));		
	//  }		
//		function saveFrame(){	
//			$.ajax({ 		
//		 		url:'/project/stitch-snaps/', 		
//		 		type:'POST', 		
//		 		data: { pid: $('#project_id').val() }, 		
//		 		success: function(data) { 
//		 			$(window).unbind('beforeunload'); console.log(data); window.location.href=window.location.origin+'/my-projects/'; },		
//		 		error:function(req,stat,err){ console.log(req.responseText); },		
//		        cache: false,
//		        xhr: function () { 
//		        	var xhr = $.ajaxSettings.xhr(); 
//		        	if (xhr.upload) { 
//		        		xhr.upload.addEventListener('progress', function (evt) { 
//		        			var percent = Math.floor((evt.loaded / evt.total) * 100),dataPerc=100-percent; 
//		        			$('.progress').find('.progress-bar').css('width',percent+'%'); 
//		        			console.log(percent);
//		        		}, false); 
//		        	} 
//		        	return xhr;
//		        },
//		 		headers:{"X-CSRFToken": $('input[name="csrfmiddlewaretoken"]').attr('value')}		
//		 	});		
//		}		
				
		function submitFrames(canv,ind){
			var end = 1;//ind<mini_canvas.length?0:1;
			$.ajax({ 		
	 		url:'/project/save-snaps/', 		
	 		type:'POST', 		
	 		data: { snaps: JSON.stringify(frames), pid: $('#project_id').val(), ind: ind, end: end }, 		
	 		success: function(data) { console.log(data); frames=[];
		 		if(end){
					$(window).unbind('beforeunload'); console.log(data); window.location.href=window.location.origin+'/my-projects/';
				}else{	
					ind++;
					letAnim(canv,ind,10000);
				}
	 		},		
	 		error:function(req,stat,err){ console.log(req.responseText); },		
	 		headers:{"X-CSRFToken": $('input[name="csrfmiddlewaretoken"]').attr('value')}		
	 	});		
		}		
	 function letAnim(canv,ind,duration){		console.log(ind);
		 canv.loadFromJSON(JSON.stringify(tempJson.frames.data[ind].fabric), 
			function(){ canv.renderAll.bind(canv); 
						primfeat=[canv.item(0).scaleX,canv.item(0).scaleY,canv.item(0).left]; 	
						  //var ii=0; setInt=setInterval(function(){ console.log(ii); ii++; if(ii>duration/1000){ clearInterval(setInt); } },1000);
						  var strips=[],colorArr=['#fff','#eee','#ddd','#ccc','#bbb'];		
						   for(i=0;i<5;i++){		
							   strips[i] = getRects((128*i),colorArr[i]),		
							   canv.add(strips[i]);		
						   }		
						   canv.renderAll();
						 	$.each(strips,function(j,k){	
						     	animStrips(canv,j,k,strips.length,duration,ind);		
						 	});		
					});
							
	 }		
	 function letAnim1(){			
		  var strips=[],colorArr=['#fff','#eee','#ddd','#ccc','#bbb','#aaa'];		
		   for(i=0;i<6;i++){		
			   strips[i] = getRects1((165*i),colorArr[i]),		
			   canvas.add(strips[i]);		
		   }		
		   canvas.renderAll();		
		   		
		   canvas.item(0).set({scaleX:primfeat[0],scaleY:primfeat[1],left:primfeat[2]}); canvas.renderAll();		
	 			
	 	canvas.item(0).animate({scaleX:1.1,scaleY:1.1,left:350}, {		
	 	      duration: 2000,		
	 	      onChange: canvas.renderAll.bind(canvas),		
	 	      easing: fabric.util.ease[6]		
	 	    });		
	 	$.each(strips,function(j,k){ console.log(k);		
	     	animStrips1(j,k);		
	 	});		
	 }