 	//load data from Template JSON to Mini Canvases
    load_min_canvas = function() {
    	//updateJson();
    	$('.quick-editor').find('.row .col-md-4').not($('#frame_addition').parent()).remove();
		$.each(tempJson.frames.data, function(i,j){
			var blkHtml='<div class="col-md-4">\n\
						   <div class="card shadow mb-4">\n\
						      <!-- Card Header - Dropdown -->\n\
						      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">\n\
						         <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-crop-alt fa-fw"></i> Frame '+(i+1)+'</h6>\n\
						         <div class="dropdown no-arrow">\n\
						            <a href="javascript:void(0)" class="copy-frame btn btn-success btn-circle btn-sm" role="button" data-id="'+i+'" data-toggle="tooltip" title="Copy this Video Frames">\n\
						            <i class="fas fa-clone"></i>\n\
						            </a>\n\
						            <a href="javascript:void(0)" class="delete-frame btn btn-danger btn-circle btn-sm" role="button" data-id="'+i+'" data-toggle="tooltip" title="Delete this Video Frames">\n\
						            <i class="fas fa-trash"></i>\n\
						            </a>\n\
						         </div>\n\
						      </div>\n\
						      <!-- Card Body -->\n\
							<div class="card-body">\n\
						   		<div style="display: flex; justify-content: center;">\n\
						   		</div>\n\
							</div>\n\
						      <!-- Card Footer -->\n\
						      <div class="card-footer">\n\
							     <a href="javascript:void(0)" class="btn btn-light btn-icon-split btn-sm" style="border: 1px solid #606b7d;">\n\
							         <span class="icon text-gray-800">\n\
							         <i class="fas fa-clock fa-fw"></i>\n\
							         </span>\n\
							         <span class="text">\n\
							         <b> '+ tempJson.frames.data[i].duration +' Sec </b>\n\
							         </span>\n\
						         </a>\n\
						         <a href="javascript:void(0)" class="btn btn-light btn-icon-split btn-sm" style="border: 1px solid #606b7d;">\n\
							         <span class="icon text-gray-800">\n\
							         <i class="fas fa-'+(j.type == 'photo'?'images':(j.type == 'video'?'video':(j.type == 'text'?'tenge':(j.type == 'collage'?'th-large':'trademark'))))+' fa-fw"></i>\n\
							         </span>\n\
							         <span class="text">\n\
							         <b> '+j.type.toUpperCase()+' </b>\n\
							         </span>\n\
						         </a>\n\
						         <a href="javascript:void(0)" data-type="'+j.type+'" class="frame btn btn-info btn-icon-split btn-sm" style="float: right" data-id="'+i+'" data-type="photo">\n\
							         <span class="icon text-gray-100">\n\
							         <i class="fas fa-pencil-alt"></i>\n\
							         </span>\n\
							         <span class="text text-gray-100">Edit</span>\n\
						         </a>\n\
						      </div>\n\
						   </div>\n\
						   <!-- /.card shadow mb-4 -->\n\
						</div>';
			
			$(blkHtml).insertBefore($('.quick-editor').children('.row').children().last());
		   	var ratio, canvCard=$('.quick-editor').children('.row').children().last().prev();
		   	
		    //Initialising the functions of Mini Canvas, After loading
		   	initMiniCanv(canvCard);
		   	
		   	//Count Total Duration of All Frames
		   	totalDuration[i] = tempJson.frames.data[i].duration;
		   	$("#totalduration").find('.text').html("<b>" + eval(totalDuration.join("+")) + " Sec </b>");
	    	
			//creating dynamic canvas object since append() makes fabric get confused
			var canvas_node = document.createElement('canvas');
			canvas_node.id = "mini_canvas"+i;
			canvas_node.className = "mini-canvas";
			canvas_node.height = 180;
			canvas_node.width = 288;
			canvas_node.setAttribute('style',"border: 1px solid #000; box-shadow: 5px 8px 8px -2px;");
			
			mini_canvas[i] = new fabric.Canvas(canvas_node, { preserveObjectStacking: true });
			canvCard.find('.card-body div').append(mini_canvas[i].wrapperEl);			
			ratio = mini_canvas[i].width/645;
			mini_canvas[i].loadFromJSON(JSON.stringify(j.fabric),
			function(){
				mini_canvas[i].renderAll.bind(mini_canvas[i]);
				mini_canvas[i].set('backgroundColor', j.color.background);
				mini_canvas[i].renderAll();
				//if the frame is a video frame we need to start the video stream
	        	if(j.type=='video'){
	    			load_video(mini_canvas[i],0);
	    		}else if(j.type=='collage'){
	    			load_collage(mini_canvas[i]);
	    		}
	    	}, 
	 		function(o, object) {
	     		object.scaleX=object.scaleX*ratio;
	     		object.scaleY=object.scaleY*ratio;
	     		object.top=object.top*ratio;
	     		object.left=object.left*ratio;
	     		object.set('selectable', false); 
	     		object.set('editable', false);
	 		}); 
		});
		
	}
    
    
    //Populate Changes in forms Life long
    function populateForm(data){
    	var ec = $('.editor'),
    	duration = ec.find('.durationSlider'),
    	headingText = ec.find('.headingText'),
    	subheadingText = ec.find('.subheadingText'),
    	headingTextSize = ec.find('.headingTextSlider'),
    	subheadingTextSize = ec.find('.subheadingTextSlider'),
    	headingTextColor = ec.find('#titleColor'),
    	subheadingTextColor = ec.find('#subTitleColor'),
    	headingBGColor = ec.find('#titleBgColor'),
    	subheadingBGColor = ec.find('#subTitleBgColor'),
		ind = data.type == 'collage' ? data.media.url.length*2 : 1;
    	
    	
    	duration.val(data.duration);
    	duration.prev().children().text(data.duration);
    	
    	headingText.val(data.fabric.objects[ind].text);
    	
    	subheadingText.val(data.fabric.objects[ind+1].text);
    	
    	headingTextSize.val(data.fabric.objects[ind].fontSize);
    	headingTextSize.prev().children().text(data.fabric.objects[ind].fontSize);
    	
    	subheadingTextSize.val(data.fabric.objects[ind+1].fontSize);
    	subheadingTextSize.prev().children().text(data.fabric.objects[ind+1].fontSize);
    	
    	if (data.fabric.objects[ind].fontWeight == "bold") { $(".text-style[data-activity='title-bold']").addClass("btn-info"); } else { $(".text-style[data-activity='title-bold']").addClass("btn-secondary"); }
    	
    	if (data.fabric.objects[ind+1].fontWeight == "bold"){ $(".text-style[data-activity='subtitle-bold']").addClass("btn-info") } else { $(".text-style[data-activity='subtitle-bold']").addClass("btn-secondary"); }
    	
    	if (data.fabric.objects[ind].underline == true){ $(".text-style[data-activity='title-underline']").addClass("btn-info"); } else { $(".text-style[data-activity='title-underline']").addClass("btn-secondary"); }
    	
    	if (data.fabric.objects[ind+1].underline == true){ $(".text-style[data-activity='subtitle-underline']").addClass("btn-info"); } else { $(".text-style[data-activity='subtitle-underline']").addClass("btn-secondary"); }
    	
    	if (data.fabric.objects[ind].fontStyle == "italic"){ $(".text-style[data-activity='title-italic']").addClass("btn-info"); } else { $(".text-style[data-activity='title-italic']").addClass("btn-secondary"); }
    	
    	if (data.fabric.objects[ind+1].fontStyle == "italic"){ $(".text-style[data-activity='subtitle-italic']").addClass("btn-info"); } else { $(".text-style[data-activity='subtitle-italic']").addClass("btn-secondary"); }
    	
    	headingTextColor.val(data.fabric.objects[ind].fill);
    	
    	subheadingTextColor.val(data.fabric.objects[ind+1].fill);
    	
    	headingBGColor.val(data.fabric.objects[ind].textBackgroundColor);
    	
    	subheadingBGColor.val(data.fabric.objects[ind+1].textBackgroundColor);
    	
    	ec.find('#photoScale').val(data.fabric.objects[0].scaleX);
    	ec.find('#photoScale-value').text(data.fabric.objects[0].scaleX);
    	
    	$('.video-trimmer').data({
    		'path': tempJson.frames.data[miniCanvasIndex].fabric.objects[0].video_src,
    		'duration': tempJson.frames.data[miniCanvasIndex].fabric.objects[0].video_duration,
    		'height': tempJson.frames.data[miniCanvasIndex].fabric.objects[0].video_height,
    		'width': tempJson.frames.data[miniCanvasIndex].fabric.objects[0].video_width
    	});
    }

    //Getting Video Element
    function getVideoElement(obj) {
        var videoE = document.createElement('video');
        videoE.width  = obj.video_width;
        videoE.height = obj.video_height;
        videoE.muted = true;
        videoE.crossOrigin = "anonymous";
        var source = document.createElement('source');
        source.src = obj.video_src;
        source.type = 'video/mp4';
        videoE.appendChild(source);
        return videoE;
    }

    //Load Videos on Canvas
    function load_video(canv,play){
    	video1El = getVideoElement(canv.item(0));
    	var scale=video1El.width/video1El.height > canv.width/canv.height ? canv.height/video1El.height : canv.width/video1El.width; 
        var video1 = new fabric.Image(video1El, {
          left: canv.width/2, top: canv.height/2,
          originX:'center', originY:'center',
          scaleX: scale, scaleY: scale,
          video_src:canv.item(0).video_src,
          video_duration:canv.item(0).video_duration,
          video_width:canv.item(0).video_width,
          video_height:canv.item(0).video_height  
        });
        canv.remove(canv.item(0)); 
        canv.add(video1);
		canv.item(0).bringToFront();
		canv.item(0).bringToFront();
		canv.item(0).bringToFront();
		if(play==1){
    	video1El.play();       
        fabric.util.requestAnimFrame(function render() {
//            var image = canv.item(0);
//            var backend = fabric.filterBackend;
//            if (backend && backend.evictCachesForKey) {
//              backend.evictCachesForKey(image.cacheKey);
//              backend.evictCachesForKey(image.cacheKey + '_filtered');
//            }
//            image.applyFilters();
            canv.renderAll();
            fabric.util.requestAnimFrame(render);
          });
		}
    }
    
    //Bind clipping area to the loaded object
    function findByClipName(canv,name) {
	    return _(canv.getObjects()).where({
	            clipFor: name
	        }).first()
	}
    
    //
	function degToRad(degrees) {
	    return degrees * (Math.PI / 180);
	}

	var clipByName = function (ctx) {
        var img_to_clip=this[1];
        var clipRect = findByClipName(this[0],img_to_clip.clipName);
        var scaleXTo1 = (1 / img_to_clip.scaleX);
        var scaleYTo1 = (1 / img_to_clip.scaleY);
        ctx.save();
        ctx.translate(0,0);
        //logic for correct scaling
        if (img_to_clip.flipY && !img_to_clip.flipX){
          ctx.scale(scaleXTo1, -scaleYTo1);
        } else if (img_to_clip.flipX && !img_to_clip.flipY){
          ctx.scale(-scaleXTo1, scaleYTo1);
        } else if (img_to_clip.flipX && img_to_clip.flipY){
          ctx.scale(-scaleXTo1, -scaleYTo1);
        } else {
        	ctx.scale(scaleXTo1, scaleYTo1);
        }
        
        //IMPORTANT!!! do rotation after scaling
        ctx.rotate(degToRad(img_to_clip.angle * -1));
        ctx.beginPath();
        ctx.rect(
            clipRect.left - img_to_clip.left,
            clipRect.top - img_to_clip.top,
            clipRect.width,
            clipRect.height
        );
        ctx.closePath();
        ctx.restore();
    }
	
    //Load Collage on Canvas
    function load_collage(canv){
    	var imgArr=[canv.item(4),
    		canv.item(5),
    		canv.item(6),
    		canv.item(7)],
    	imgEl=[],imgObj=[];var collageClips=[];
    	fabric.Group.prototype.perPixelTargetFind = false;
    	
    	canv.remove(canv.item(4));
    	canv.remove(canv.item(4));
    	canv.remove(canv.item(4));
    	canv.remove(canv.item(4));
    	
    	$.each(imgArr,function(i,j){

//    	collageClips[i] = new fabric.Rect({
//    	    left: (i%2==0?0:320),
//    	    top: (i>1?200:0),
//    	    width: 320,
//    	    height: 200,
//    	    fill: 'transparent',
//    	    opacity: 1,
//    	    selectable: false
//    	});
//    	collageClips[i].set({ clipFor: 'img'+i });
//    	canv.add(collageClips[i]);

    	imgEl[i] = new Image();
    	imgEl[i].onload = function (img) {    

    	//var scale=imgEl[i].width/imgEl[i].height > canv.width/canv.height ? canv.height*0.5/imgEl[i].height : canv.width*0.5/imgEl[i].width; 

    	    imgObj[i] = new fabric.Image(imgEl[i], {
    	    	angle: 0,
    	        left: j.left,
    	        top: j.top,
    	        flipX: false,
    	        flipY: false,
    	        scaleX: j.scaleX,
    	        scaleY: j.scaleY,
    	        originX: j.originX,
    	        originY: j.originY,
    	        clipName: j.clipName,
    	        clipTo: function(ctx) { return _.bind(clipByName, [canv,imgObj[i]])(ctx) }
    	    });
    	    canv.add(imgObj[i]);
    	    if(i==3){ 
    			canv.item(4).bringToFront();
    			canv.item(4).bringToFront();
    			canv.item(4).bringToFront();
    		}
    	};
    	imgEl[i].crossOrigin = 'Anonymous';
    	imgEl[i].src = j.src;
    	});
    }
    
    //Load Collage on Canvas
    function load_collage1(canv){
    	fabric.Group.prototype.perPixelTargetFind = false
    	for(i=4;i<=7;i++){
    		canv.item(i).clipTo = function(ctx) { return _.bind(clipByName, canv.item(i))(ctx) }
    	}
    	imgEl[i].src = j;
    }
	
    
    //Initialising the functions of Mini Canvas, After loading
    function initMiniCanv(canvCard){
    	
       	//onClick showing .popup-editor
       	canvCard.find(".frame").click(function(){
        	setTimeout(function(){ $('.popup-editor').fadeIn(); },1000);
        	$('.quick-editor').fadeOut();
        	
        	var type = $(this).data('type');
        	
    		if ( type=="photo" ) {
    			$(".type-photo").show();
    			$(".type-video").hide();
    			$(".type-collage").hide();
    			$(".type-solidbackground").hide();
    			$(".make-bg-image-sel").show();
    			$(".type-duration").find('.durationSlider').show();
    		} else if ( type=="video" ) {
    			$(".type-photo").hide();
    			$(".type-video").show();
    			$(".type-collage").hide();
    			$(".type-solidbackground").hide();
    			$(".make-video-sel").show();
    			$(".type-duration").find('.durationSlider').hide();
    		} else if ( type=="collage" ) {
    			$(".type-photo").hide();
    			$(".type-video").hide();
    			$(".type-collage").show();
    			$(".type-solidbackground").hide();
    			$(".type-duration").find('.durationSlider').show();
    		} else {
    			$(".type-photo").hide();
    			$(".type-video").hide();
    			$(".type-collage").hide();
    			$(".type-solidbackground").show();
    			$(".type-duration").find('.durationSlider').show();
    		}
    		
        	miniCanvasIndex=$(this).data('id');
        	
        	$('#deleteFrame').data("id", miniCanvasIndex);
        	
        	populateForm(tempJson.frames.data[miniCanvasIndex]);
        	
        	canvas.loadFromJSON(JSON.stringify(tempJson.frames.data[miniCanvasIndex].fabric), function(){
        		canvas.renderAll.bind(canvas);
            	canvas.set('backgroundColor', tempJson.frames.data[miniCanvasIndex].color.background);
            	canvas.item(canvas._objects.length - 1).set("selectable", false);
            	
				//if the frame is a video frame we need to start the video stream

            	if(tempJson.frames.data[miniCanvasIndex].type=='video'){
        			load_video(canvas,1);
        		}else if(tempJson.frames.data[miniCanvasIndex].type=='collage'){
	    			load_collage(canvas);
	    		}
         	   
				primfeat=[canvas.item(0).scaleX,canvas.item(0).scaleY,canvas.item(0).left];

            	canvas.renderAll();
        		
        	} , function(o, object) { 
        			object.set('stroke', '#59fee8');
        			object.set('strokeWidth', 0);
        			object.set('selectable', true); object.set('editable', false);
        	}); 
        });
       	
        //Frame Clone function
       	canvCard.find('.copy-frame').click(function() {
    		var x = $(this).data('id');
    		
    		if ((tempJson.frames.data[x].duration + eval(totalDuration.join("+"))) <= 90 ) {
	    		var temp = JSON.stringify(tempJson.frames.data[x]);
	    		tempJson.frames.data.splice(x+1, 0, JSON.parse(temp));
	    		load_min_canvas();
    		} else {
    			show_toast("The Duration Limit reached and try to reduce some.");
    		}
    	});
    	
       	//Frame Deletion function from inside
       	$('#deleteFrame').click(function() {
       		$("#frameDeletiontModal").find(".delete-frame-confirm").data('id',$(this).data('id'));
       		$("#frameDeletiontModal").find(".delete-frame-confirm").data('callfrom',"inside");
        	$("#frameDeletiontModal").modal("show"); 		
    	});
       	
        //Frame Deletion function from outside
       	canvCard.find('.delete-frame').click(function() {
       		$("#frameDeletiontModal").find(".delete-frame-confirm").data('id',$(this).data('id'));
       		$("#frameDeletiontModal").find(".delete-frame-confirm").data('callfrom',"outside");
        	$("#frameDeletiontModal").modal("show");
       		
    	});
    }

   	
   	function getObj(canvas){
   		var json=canvas.getObjects();
   		$.each(json,function(i,j){
   			delete j['clipTo'];
   		});
   		console.log(JSON.stringify(canvas.getObjects()));
   	}