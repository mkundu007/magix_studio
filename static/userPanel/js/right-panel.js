   //Save changes of objects and their property on Canvas in Template JSON
	canvas.on('object:modified',function(e){ var inx = canvas.getObjects().indexOf(e.target); tempJson.frames.data[miniCanvasIndex].fabric.objects[inx]=canvas.item(inx);});
  	
    //Background select function
    function imagebackground_select(thisBtn) {
    	
    	var imgElement = thisBtn.parent().next()[0].src;
    	
    	canvas.item(0).setSrc(imgElement, function(background){ 
    		background.set({
		    	'scaleX': 1,
			    'scaleY': 1,
			    'top': canvas.height/2,
			    'left': canvas.width/2,
			    'angle': 0,
		    	'originX': "center",
		    	'originY': "center"
		    });
    		canvas.renderAll();
    	});
    	tempJson.frames.data[miniCanvasIndex].fabric.objects[0].src=imgElement;
		tempJson.frames.data[miniCanvasIndex].fabric.objects[0].scaleX=1;
        tempJson.frames.data[miniCanvasIndex].fabric.objects[0].scaleY=1;
        tempJson.frames.data[miniCanvasIndex].fabric.objects[0].top=canvas.height/2;
        tempJson.frames.data[miniCanvasIndex].fabric.objects[0].left=canvas.width/2;
        tempJson.frames.data[miniCanvasIndex].fabric.objects[0].angle=0;
        tempJson.frames.data[miniCanvasIndex].fabric.objects[0].originX="center";
        tempJson.frames.data[miniCanvasIndex].fabric.objects[0].originY="center";
    }
    
   //Background reset function
    function imagebackground_reset(thisBtn) {
    	canvas.item(0).set({
		    'scaleX': 1,
		    'scaleY': 1,
		    'top': canvas.height/2,
		    'left': canvas.width/2,
		    'angle': 0,
		    'originX': "center",
		    'originY': "center"
    	});
    	canvas.renderAll();
    	tempJson.frames.data[miniCanvasIndex].fabric.objects[0].scaleX=1;
        tempJson.frames.data[miniCanvasIndex].fabric.objects[0].scaleY=1;
        tempJson.frames.data[miniCanvasIndex].fabric.objects[0].top=canvas.height/2;
        tempJson.frames.data[miniCanvasIndex].fabric.objects[0].left=canvas.width/2;
        tempJson.frames.data[miniCanvasIndex].fabric.objects[0].angle=0;
        tempJson.frames.data[miniCanvasIndex].fabric.objects[0].originX="center";
        tempJson.frames.data[miniCanvasIndex].fabric.objects[0].originY="center";
        
        
    	$('.editor').find('#photoScale').val("1");
    	$('.editor').find('#photoScale-value').text("1");
    }
    

    //Video Trimming Function
    function video_trim(thisBtn) {
    	var x = parseInt(thisBtn.data('duration')), front_end = 0, rear_end = x, height = parseInt(thisBtn.data('height')), width = parseInt(thisBtn.data('width')), src = thisBtn.data('path');
    			    	
    	$("#videoTrimmerModal").find('video').attr("src",thisBtn.data('path'));
    			    	
    	$("#videoTrimmerModal").modal('show');
    	
    	$("#video-time-range").slider({
    	    range: true,
    	    min: 0,
    	    max: x,
    	    step: 1,
    	    values: [0, x],
    	    slide: function (e, ui) {
    	        var min1 = Math.floor(ui.values[0] / 60);
    	        var sec1 = ui.values[0] - (min1 * 60);
    	            
    	        if (min1.toString().length == 1){ min1 = '0' + min1; }
    	        if (sec1.toString().length == 1){ sec1 = '0' + sec1; }
    	        if (sec1 == 0) sec1 = '00';
    	        $('.slider-time1').html(min1 + ':' + sec1);
    	            
    	        
    	        var min2 = Math.floor(ui.values[1] / 60);
    	        var sec2 = ui.values[1] - (min2 * 60);
    	           
    	        if (min2.toString().length == 1) min2 = '0' + min2;
    	        if (sec2.toString().length == 1) sec2 = '0' + sec2;
    	        if (sec2 == 0) sec2 = '00';
    	        $('.slider-time2').html(min2 + ':' + sec2);
    	        
    	        if ( front_end != ui.values[0] && rear_end == ui.values[1] ) {
    	        	$("#videoTrimmerModal").find("video")[0].currentTime = ui.values[0];
    	        	front_end = ui.values[0];
    	        } else if ( front_end == ui.values[0] && rear_end != ui.values[1] ) {
		        	$("#videoTrimmerModal").find("video")[0].currentTime = ui.values[1];
		        	rear_end = ui.values[1];
		        }  
    	        
    	    }
    	});

        $('.video-trimmer').click(function() {
        	tempJson.frames.data[miniCanvasIndex].duration = (rear_end - front_end);
        	//tempJson.frames.data[miniCanvasIndex].fabric.objects[0].video_src = "/media/uploaded_media/Mai1563857692.803663@magix/magix.mp4";
        	//tempJson.frames.data[miniCanvasIndex].fabric.objects[0].video_src = "/static/VID_20190529_132728.mp4";
        	tempJson.frames.data[miniCanvasIndex].fabric.objects[0].video_src = src;
        	tempJson.frames.data[miniCanvasIndex].fabric.objects[0].video_duration = (rear_end - front_end);
        	tempJson.frames.data[miniCanvasIndex].fabric.objects[0].video_height = height;
        	tempJson.frames.data[miniCanvasIndex].fabric.objects[0].video_width = width;
        	
        	//Count Total Duration of All Frames
		   	totalDuration[miniCanvasIndex] = tempJson.frames.data[miniCanvasIndex].duration;
		   	$('#totalduration').find('.text').html("<b>" + eval(totalDuration.join("+")) + " Sec </b>");
		   	
		   	$('#durationLength-value').text(tempJson.frames.data[miniCanvasIndex].duration);
		   	
        	canvas.loadFromJSON(JSON.stringify(tempJson.frames.data[miniCanvasIndex].fabric), function(){
        		canvas.renderAll.bind(canvas);
            	canvas.set('backgroundColor', tempJson.frames.data[miniCanvasIndex].color.background);
            	canvas.item(canvas._objects.length - 1).set("selectable", false);
				//if the frame is a video frame we need to start the video stream
            	if(canvas.item(0).video_src){
        			load_video(canvas);
        		}
            	canvas.renderAll();
        		
        	} , function(o, object) { 
        			object.set('stroke', '#59fee8');
        			object.set('strokeWidth', 0);
        			object.set('selectable', true); object.set('editable', false);
        	}); 
        });
    }