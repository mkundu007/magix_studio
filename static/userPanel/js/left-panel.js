	
  	//File upload function
    function file_upload(form){     
	   var formData = new FormData(form[0]), 
	   	   form_parents = form.parents('.side-nav'), 
	   	   current_upload = form_parents.find('.current-upload'), 
	   	   no_upload_status = form_parents.find('.no-upload-status'),
	   	   upload_button = form_parents.find('.uploadsubmit'); 
	   
	   var url_temp = "";
	   
	   if ( form.data('type') == "video" ) { url_temp = "/video/upload/"; } else { url_temp = "/media/upload/"; }
	   
	   $.ajax({
	        type:'POST',
	        url: url_temp, 
	        data:formData,
	        processData: false,
	        contentType: false,
	        
	        //For progress-bar update
	        cache: false,
	        xhr: function () { 
	        	var xhr = $.ajaxSettings.xhr(); 
	        	if (xhr.upload) { 
	        		xhr.upload.addEventListener('progress', function (evt) { 
	        			var percent = Math.floor((evt.loaded / evt.total) * 100),dataPerc=100-percent; 
	        			$('.progress').find('.progress-bar').css('width',percent+'%'); }, false); 
	        		} 
	        	return xhr;
	        },
	        
	        success: function(response) {
	        	form[0].reset();
				
	        	if (response['thumbnail']) {     
	        		
	        		if (response['type']) {
	        			//Reflect currently uploaded Videos
	        			new_upload=
				      		'<div>\n\
		    					<div class="side-nav-activity" data-del=' + response['media_id'] + ' > \n\
								 	<a class="fa fa-star side-nav-activity-btn make-fav"></a> \n\
								 	<a class="fa fa-play side-nav-activity-btn make-video-play" data-path="' + response['path'] + '" data-duration="' + response['duration'] + '"></a> \n\
								    <a class="fa fa-trash side-nav-activity-btn make-del"></a> \n\
								 	<a class="fa fa-check side-nav-activity-btn make-video-sel" data-path="' + response['path'] + '" data-duration="' + response['duration'] + '" data-width="' + response['width'] + '" data-height="' + response['height'] + '" data-toggle="tooltip" title="Delete this Video" style="padding: 10px;"></a> \n\
								</div> \n\
								<img  class="img-fluid img-thumbnail side-thumbnail" src=" ' + response['thumbnail'] + ' " />\n\
							</div>'
	        		} else {
	        			//Reflect currently uploaded Audio
				      	new_upload=
				      		'<div>\n\
		    					<div class="side-nav-activity" data-del=' + response['media_id'] + ' > \n\
								 	<a class="fa fa-star side-nav-activity-btn make-fav"></a> \n\
								 	<a class="fa fa-play side-nav-activity-btn make-audio-play" data-path="' + response['path'] + ' "data-duration="'  + response['duration'] + '" ></a> \n\
								    <a class="fa fa-trash side-nav-activity-btn make-del"></a> \n\
								    <a class="fa fa-check side-nav-activity-btn make-audio-sel" data-path="' + response['path'] + ' "data-duration="'  + response['duration'] + '" ></a> \n\
								</div> \n\
								<img  class="img-fluid img-thumbnail side-thumbnail" src=" ' + response['thumbnail'] + ' " />\n\
							</div>'
	        		}
	        		
	        		//Activities after currently uploaded media
			      	if ( current_upload.children().length > 0 ){
			      		$(new_upload).insertBefore(current_upload.children().first());
			      	} else{
			      		current_upload.append(new_upload);
			      	}
			      	
			      	if (canvas.getObjects().length>0 && tempJson.frames.data[miniCanvasIndex].type=="photo") {
			      		$(".make-video-sel").show();
			      	} else {
			      		$(".make-video-sel").hide();
			      	}
			      	
			      	upload_button.attr('disabled', false);
			      	upload_button.html('<i class="fas fa-upload"></i> Upload Again');
			      	
			      	setTimeout( function(){ $('.progress').find('.progress-bar').css('width','0%') }, 1000);
					$('.progress').css("opacity", "0");
			      	
			      	$('.current-upload').find('img').mouseenter(function(){ $(this).prev().addClass('active')});
			    	$('.side-nav-activity').mouseleave(function(){ $(this).removeClass('active')});
			    	
			    	
			    	
			    	//instant file delete function 
			      	current_upload.children().first().find($(".make-del")).click(function(e){
			      		$("#mediaDeletiontModal").find(".make-del-confirm").data('del',$(this).parent().data('del'));
			        	$("#mediaDeletiontModal").modal("show");
			        	
			        });
			      	
			      	//Instant audio play function
			      	current_upload.children().first().find($(".make-audio-play")).click(function(e){
			      		e.preventDefault();
			        	
			        	audio_play($(this));
			        });
			      	
			      	//Instant to Audio Trimmer appearance
			    	$('.make-audio-sel').click(function(e){
			        	
			        	audio_trim($(this));
			        	
			        });
			    	
			      	//Instant video play function
			      	current_upload.children().first().find($(".make-video-play")).click(function(e){
			      		e.preventDefault();
			        	
			        	video_play($(this));
			         });
			      	
			      	 //Instant call to Video Trimmer appearance
			    	$('.make-video-sel').click(function(e){
			        	
			        	video_trim($(this));
			        	
			        });
			      	
			      	if ( no_upload_status.length > 0 ){
			      		 no_upload_status.fadeOut();
			      	}
	        	}
	        	
	        	else {     
	        		
	        		//Reflect currently uploaded Images
	        		if (response['type'] == "media_image") {
				      	new_upload=
				      		'<div>\n\
		    					<div class="side-nav-activity" data-del=' + response['media_id'] + ' > \n\
								 	<a class="fa fa-star side-nav-activity-btn make-fav"></a> \n\
								    <a class="fa fa-trash side-nav-activity-btn make-del"></a> \n\
								    <a class="fa fa-check side-nav-activity-btn make-bg-image-sel"></a> \n\
								</div> \n\
								<img  class="img-fluid img-thumbnail side-thumbnail" src=" ' + response['path'] + ' "/>\n\
							</div>'
	        		} else {
	        			//Reflect currently uploaded Watermarks
	        			new_upload=
				      		'<div>\n\
		    					<div class="side-nav-activity" data-del=' + response['media_id'] + ' > \n\
								 	<a class="fa fa-star side-nav-activity-btn make-fav"></a> \n\
								    <a class="fa fa-trash side-nav-activity-btn make-del"></a> \n\
								    <a class="fa fa-check side-nav-activity-btn make-watermark-sel"></a> \n\
								</div> \n\
								<img  class="img-fluid img-thumbnail side-thumbnail" src=" ' + response['path'] + ' "/>\n\
							</div>'
	        		}
	        		
	        		//Activities after currently uploaded media
			      	if ( current_upload.children().length > 0 ){
			      		$(new_upload).insertBefore(current_upload.children().first());
			      	}
			      	else{
			      		current_upload.append(new_upload);
			      	}
			      	
			      	if (canvas.getObjects().length>0 && tempJson.frames.data[miniCanvasIndex].type=="photo") {
			      		$(".make-bg-image-sel").show();
			      	} else {
			      		$(".make-bg-image-sel").hide();
			      	}
						
			      	upload_button.attr('disabled', false);
			      	upload_button.html('<i class="fas fa-upload"></i> Upload Again');
			      	
			      	if (upload_button.hasClass("load_it")){
			      		imagebackground_select($('.side-nav-activity').eq(0).find('.make-bg-image-sel'));
			      		upload_button.removeClass("load_it");
			      	}
			      	
			      	$('.progress').find('.progress-bar').css('width','0%');
					$('.progress').css("opacity", "0");
					
			      	$('.current-upload').find('img').mouseenter(function(){ $(this).prev().addClass('active')});
			    	$('.side-nav-activity').mouseleave(function(){ $(this).removeClass('active')});
			    	
			    	
			    	
			    	//instant file delete function 
			      	current_upload.children().first().find($(".make-del")).click(function(e){
			      		$("#mediaDeletiontModal").find(".make-del-confirm").data('del',$(this).parent().data('del'));
			        	$("#mediaDeletiontModal").modal("show");
			        	
			        });
			    	
			    	//onClick image select or background select on canvas function call
			    	$(".make-bg-image-sel").click(function(e){
			        	//e.preventDefault();
			        	
			        	//image_select($(this));
			        	imagebackground_select($(this));
			        	
			        	tempJson.frames.data[miniCanvasIndex].fabric.objects[0].src=$(this).parent().next()[0].src;
			        });
			    	
			    	$(".make-watermark-sel").click(function(e){
			        	//e.preventDefault();
			        	
			        	watermark_select($(this));
			        });
			      	 	
			      	if ( no_upload_status.length > 0 ) {
			      		no_upload_status.fadeOut();
			      	}
	        	}
	        },
	        
	        error:function(request,status,error) {
		      	upload_button.removeAttr('disabled');
		      	upload_button.html('<i class="fas fa-upload"></i> Upload '+form.data('type').charAt(0).toUpperCase()+form.data('type').substring(1)+'s');
		      	setTimeout( function(){ $('.progress').find('.progress-bar').css('width','0%') }, 1000);
				$('.progress').css("opacity", "0");
    			show_toast("Oops! Something unexpected happened. Please try again later");
	        },
	        
	        headers:{"X-CSRFToken": $('input[name="csrfmiddlewaretoken"]').attr('value')}
	  });
	}
    
  	//File delete function
    function file_del(thisBtn){
    	$.ajax({ 
    		url:'/media/delete/', 
    		type:'POST', 
    		data: {
    			media_id: thisBtn.data('del'),
    		}, 
    		
    		success: function(data) { 
    			
    			if ( $(".side-nav-activity[data-del =" + thisBtn.data('del') + "]").parents('.current-upload').children().length == 1 ){
    				 $(".side-nav-activity[data-del =" + thisBtn.data('del') + "]").parents('.side-nav').find('.no-upload-status').fadeIn();
		      	}
    			
    			$(".side-nav-activity[data-del =" + thisBtn.data('del') + "]").parent().fadeOut(300, function(){
    				$(this).remove();
    			});
    			$("#mediaDeletiontModal").modal("hide"); 
    			
    		},
    		
    		error: function() { 
    			$("#mediaDeletiontModal").modal("hide"); 
    			show_toast("Oops! Something unexpected happened. Please try again later");
    		},
    		
    		headers:{"X-CSRFToken": $('input[name="csrfmiddlewaretoken"]').attr('value')}
    	});
    }
    
  	//Audio play fuction
    function audio_play(thisBtn) {
    	//console.log("playing audio...");
    	
    	$("#audioModal").find("audio").attr("src",thisBtn.data('path'));
    	
    	$("#audioModal").modal("show");
    }
  	
  	//Video play fuction
    function video_play(thisBtn) {
    	//console.log("playing video...");
    	
    	$("#videoModal").find("video").attr("src",thisBtn.data('path'));
    	
    	$("#videoModal").modal("show");
    }
    

    //Extra image insertion function
    function image_insertion(thisBtn) {
    	
    	var imgElement = thisBtn.parent().next()[0];
    	var imgInstance = new fabric.Image(imgElement, {
    	  left: 10,
    	  buttom: 10,
    	  angle: 0,
    	  opacity: 0.5
    	});
    	canvas.add(imgInstance);
    	canvas.renderAll();
    }
    
    //Watermark select function
    function watermark_select(thisBtn) {
    	watermarkSizeChangeLimit = 1;
		tempJson.design.watermark.position="top-left";
    	var imgElement = thisBtn.parent().next()[0].src;
		$.each(mini_canvas, function(i, j) { 
			var canvas_ratio, aspect_ratio, actualheight, actualwidth, targetheight, targetwidth, last_item_index = (j._objects.length - 1);console.log(last_item_index);
			
    		j.item(last_item_index).setSrc(imgElement, function(watermark) {
    			canvas_ratio = j.width/645;
    			aspect_ratio = watermark.width/watermark.height;
	    		actualheight = watermark.height;
	    		targetheight = 50;
	    		actualwidth = watermark.width;
	    		targetwidth = (targetheight * aspect_ratio);
    		    watermark.set({ 
    		    	'scaleX': (targetwidth/actualwidth)*canvas_ratio,
	    			'scaleY': (targetheight/actualheight)*canvas_ratio,
    		    	'left': ((targetwidth/2)+20)*canvas_ratio,
    		    	'top': ((targetheight/2)+20)*canvas_ratio,
    		    	'opacity': 0.75
    		    });
    		    j.renderAll(); 
    		    tempJson.frames.data[i].fabric.objects[last_item_index].src=imgElement;
    		    tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=(targetwidth/actualwidth);
    		    tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=(targetheight/actualheight);
    		    tempJson.frames.data[i].fabric.objects[last_item_index].top=(targetheight/2)+20;
    		    tempJson.frames.data[i].fabric.objects[last_item_index].left=(targetwidth/2)+20;
    		    tempJson.frames.data[i].fabric.objects[last_item_index].opacity=0.75;
    		}); 		    
    	});
		
		if (canvas._objects.length != 0) {
	    	canvas.item(canvas._objects.length - 1).setSrc(imgElement, function(watermark) {
    			aspect_ratio = watermark.width/watermark.height;
	    		actualheight = watermark.height;
	    		targetheight = 50;
	    		actualwidth = watermark.width;
	    		targetwidth = (targetheight * aspect_ratio);
    		    watermark.set({ 
    		    	'scaleX': (targetwidth/actualwidth),
	    			'scaleY': (targetheight/actualheight),
    		    	'left': (targetwidth/2)+20,
    		    	'top': (targetheight/2)+20,
    		    	'opacity': 0.75
    		    });
    		    canvas.renderAll(); 
    		    $.each(mini_canvas, function(i, j) { 
    			    var last_item_index = (j._objects.length - 1);
    			    tempJson.frames.data[i].fabric.objects[last_item_index].src=imgElement;
    			    tempJson.frames.data[i].fabric.objects[last_item_index].scaleX=(targetwidth/actualwidth);
    				tempJson.frames.data[i].fabric.objects[last_item_index].scaleY=(targetheight/actualheight);
    				tempJson.frames.data[i].fabric.objects[last_item_index].top=(targetheight/2)+20;
    				tempJson.frames.data[i].fabric.objects[last_item_index].left=(targetwidth/2)+20;
    				tempJson.frames.data[i].fabric.objects[last_item_index].opacity=0.75;
    		    });
    		});
		}
    }

    
    //Audio Trimming Function
    function audio_trim(thisBtn) {
    	var x = parseInt(thisBtn.data('duration')), front_end = 0, rear_end = x;
    			    	
    	$("#audioTrimmerModal").find("audio").attr("src",thisBtn.data('path'));
    			    	
    	$("#audioTrimmerModal").modal("show");
    	
    	$("#audio-time-range").slider({
    	    range: true,
    	    min: 0,
    	    max: x,
    	    step: 1,
    	    values: [0, x],
    	    slide: function (e, ui) {
    	        var min1 = Math.floor(ui.values[0] / 60);
    	        var sec1 = ui.values[0] - (min1 * 60);
    	            
    	        if (min1.toString().length == 1){ min1 = '0' + min1; }
    	        if (sec1.toString().length == 1){ sec1 = '0' + sec1; }
    	        if (sec1 == 0) sec1 = '00';
    	        $('.slider-time1').html(min1 + ':' + sec1);
    	        $("#audioTrimmerModal").find("audio")[0].currentTime = ui.values[0];
    	            
    	        
    	        var min2 = Math.floor(ui.values[1] / 60);
    	        var sec2 = ui.values[1] - (min2 * 60);
    	           
    	        if (min2.toString().length == 1) min2 = '0' + min2;
    	        if (sec2.toString().length == 1) sec2 = '0' + sec2;
    	        if (sec2 == 0) sec2 = '00';
    	        $('.slider-time2').html(min2 + ':' + sec2);
    	        
    	        if ( front_end != ui.values[0] && rear_end == ui.values[1] ) {
    	        	$("#videoTrimmerModal").find("video")[0].currentTime = ui.values[0];
    	        	front_end = ui.values[0];
    	        } else if ( front_end == ui.values[0] && rear_end != ui.values[1] ) {
		        	$("#audioTrimmerModal").find("audio")[0].currentTime = ui.values[1];
		        	rear_end = ui.values[1];
		        }
    	    }
    	});
    }