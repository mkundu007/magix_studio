rm -r users/__pycache__/
rm -r users/migrations/__pycache__/
rm -r visitors/__pycache__/
rm -r visitors/migrations/__pycache__/
rm -r magix_studio/__pycache__/

./cloud_sql_proxy -instances=octopus-python-01:us-central1:octopus-pg-db=tcp:5433&

uwsgi --stop /tmp/magix.pid
uwsgi --ini magix.ini

