"""magix_studio URL Configuration... Author: Mainak kundu

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import epoch

from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.views.generic.base import TemplateView
from django.conf import settings
from users.views import *
from visitors.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    
    #################################################[ Check Designs of Templates ]#################################################
    path('error/', TemplateView.as_view(template_name="error404.html")),
    path('test/', TemplateView.as_view(template_name="test.html")),
#     path('conv/',conv,name="conv"),
   
   
    #############################[ wesite visitor and anonymous user accessible pages 0f magix_studio ]#############################

    url(r'^$', index, name='index'), 
    path('video/play/<str:vid>/', video_viewer, name="video_viewer"),   
    
#     path('format/', conv, name="conv"),   

     
    #########################################[ only user accessible pages 0f magix_studio ]#########################################
    path('dashboard/', dashboard, name='dashboard'),
    url(r'^my-projects/$', my_projects, name='my_projects'),
    url(r'^project/select-template/$', select_template, name='select_template'),
    path('project/set-template/<str:tid>/',set_template,name="set_template"),
    path('project/editor/<str:pid>/', editor, name='editor'),
    path('project/editor1/<str:pid>/', editor1, name='editor1'),
    
    url(r'^project/modify/$', ModifyProject.as_view(), name='modproject'),
    url(r'^project/save-snaps/$', SaveSnaps.as_view(), name='savesnaps'),
#     url(r'^project/stitch-snaps/$', StitchSnaps.as_view(), name='stitchsnaps'),
    url(r'^video/upload/$', VideoUploadApi.as_view(), name='upload_video'),
    url(r'^media/upload/$', MediaUploadApi.as_view(), name='upload_media'),
    url(r'^media/delete/$', MediaDeleteApi.as_view(), name='delete_media'),

    
    ############################################[ user_registration of magix_studio ]###############################################
    path('sign-up/<str:argm>/', sign_up, name='sign_up'),

    
    ################3##########################[ user_login & logout of magix_studio ]###############################################
    url(r'^login/$', login, name='login'),
    path('logout/', logout_view, name='logout'),
] + static ( settings.MEDIA_URL , document_root=settings.MEDIA_ROOT )
